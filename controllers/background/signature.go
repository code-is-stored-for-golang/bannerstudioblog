package controllers

import (
	"bannerstudioblog/models"
	"encoding/json"
	"strconv"
)

type SignatureController struct {
	baseController
}

// GetSignature
// @Title	GetSignature
// @Description Query a signature
// @Param	id 	path	int true	"Query a signature by ID"
// @Success 201 Query a signature successfully
// @Failure 403 Failed to query a signature
// @router /getSignature/:id [get]
func (s *SignatureController) GetSignature() {
	Id, err := s.GetInt(":id")
	if err != nil {
		s.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
		s.ServeJSON()
		return
	}
	var signature = models.Signature{Id: Id}
	err = s.o.Read(&signature)
	if err != nil {
		s.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		s.ServeJSON()
		return
	}
	s.Data["json"] = models.GetRevaOk(signature)
	s.ServeJSON()
}

// AddSignature
// @Title AddSignature
// @Description Add signature information
// @Param body	body	models.Signature true	"Add signature information"
// @Success 201 Add a signature successfully
// @Failure 403 Failed to add a signature
// @router /addSignature/ [post]
func (s *SignatureController) AddSignature() {
	var signature models.Signature
	json.Unmarshal(s.Ctx.Input.RequestBody, &signature)
	_, err := s.o.Insert(&signature)
	if err != nil {
		s.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		s.ServeJSON()
		return
	}
	s.Data["json"] = models.GetRevaOk(signature)
	s.ServeJSON()
}

// DeleteSignature
// @Title DeleteSignature
// @Description	Delete the signature
// @Param id path	int	true "Delete signature by ID"
// @Success 201	Succeeded in deleting the signature by ID
// @Failure 403 Failed to delete signature by ID
// @router /deleteSignature/:id [delete]
func (s *SignatureController) DeleteSignature() {
	Id, err := s.GetInt(":id")
	if err != nil {
		s.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
		s.ServeJSON()
		return
	}
	i, err := s.o.Delete(&models.Signature{Id: Id})
	if err != nil {
		s.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		s.ServeJSON()
		return
	}
	s.Data["json"] = models.GetRevaOk("成功删除" + strconv.Itoa(int(i)) + "条数据")
	s.ServeJSON()
}

// UpdateSignature
// @Title UpdateSignature
// @Description Update the signature
// @Param body body		models.Signature	true "Update the signature"
// @Success 201	Update signature successfully
// @Failure 403 Failed to update signature
// @router /updateSignature/ [put]
func (s *SignatureController) UpdateSignature() {
	var signature models.Signature
	json.Unmarshal(s.Ctx.Input.RequestBody, &signature)
	_, err := s.o.Update(&signature)
	if err != nil {
		s.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		s.ServeJSON()
		return
	}
	s.Data["json"] = models.GetRevaOk(signature)
	s.ServeJSON()
}
