package controllers

import (
	"bannerstudioblog/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/gomodule/redigo/redis"
)

type baseController struct {
	beego.Controller
	o    orm.Ormer
	pool redis.Conn
}

func (p *baseController) Prepare() {
	p.o = orm.NewOrm()
	p.pool = models.Conn()
}
