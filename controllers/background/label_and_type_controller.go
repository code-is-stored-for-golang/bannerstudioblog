package controllers

import (
	"bannerstudioblog/models"
	"bannerstudioblog/utils"
	"encoding/json"
	"github.com/astaxie/beego/logs"
	"strconv"
	"time"
)

type LabelController struct {
	baseController
}

// AddLabel
// @Title AddLabel
// @Description Add Label information
// @Param labelName	path	string true	"标签名"
// @Param uid	path	int true	"用户id"
// @Success 201 添加成功
// @Failure 403 添加失败
// @router /addLabel/ [post]
func (b *LabelController) AddLabel() {
	labelName := b.Input().Get("labelName")
	get := b.Input().Get("uid")
	uid, err2 := strconv.Atoi(get)
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "格式转换失败")
		b.ServeJSON()
		return
	}
	var label = models.Label{LabelName: labelName, Bid: &models.Blog{Id: -1}, Uid: &models.User{Id: uid}}
	_, err := b.o.Insert(&label)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		b.ServeJSON()
		return
	}
	b.Data["json"] = models.GetRevaOk(label)
	b.ServeJSON()
}

// AddType
// @Title AddType
// @Description Add Type information
// @Param typeName	path	string true	"分类名"
// @Param uid	path	int true	"用户id"
// @Success 201 添加成功
// @Failure 403 添加失败
// @router /addType/ [post]
func (b *LabelController) AddType() {
	typeName := b.Input().Get("typeName")
	get := b.Input().Get("uid")
	uid, err2 := strconv.Atoi(get)
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "格式转换失败")
		b.ServeJSON()
		return
	}
	var types = models.Type{TypeName: typeName, Bid: &models.Blog{Id: -1}, Uid: &models.User{Id: uid}}
	_, err := b.o.Insert(&types)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		b.ServeJSON()
		return
	}
	b.Data["json"] = models.GetRevaOk(types)
	b.ServeJSON()
}

// AddLabels	测试阶段
// @Title AddLabel
// @Description Add Labels information
// @Param labels	body	models.AssistObj true 	"请输入标签"
// @Success 201 添加成功
// @Failure 403 添加失败
// @router /AddLabels/ [post]
//func (b *LabelController) AddLabels() {
//	var assist models.AssistObj
//	s := ""
//	for _, b2 := range b.Ctx.Input.RequestBody {
//		s = s + string(b2)
//	}
//	beego.Error(s)
//	err := json.Unmarshal(b.Ctx.Input.RequestBody, &assist)
//	beego.Error(assist)
//	if err != nil {
//		b.Data["json"] = models.GetRevaErr(err, "获取结构体失败")
//		b.ServeJSON()
//	}
//	id := assist.BlogId
//	typeNames := assist.Types
//	for _, name := range typeNames {
//		types := models.Type{TypeName: name, BlogId: id}
//		_, err := b.o.Insert(&types)
//		if err != nil {
//			b.Data["json"] = models.GetRevaErr(err, "操作数据库失败")
//			b.ServeJSON()
//		}
//	}
//	labelNames := assist.Labels
//	for _, name := range labelNames {
//		labels := models.Label{LabelName: name, BlogId: id}
//		_, err := b.o.Insert(&labels)
//		if err != nil {
//			b.Data["json"] = models.GetRevaErr(err, "操作数据库失败")
//			b.ServeJSON()
//		}
//	}
//	b.Data["json"] = models.GetRevaOk(assist)
//	b.ServeJSON()
//}

// GetAssist	测试阶段
// @Title GetAssist
// @Description 获取Assist结构体
// @Success 201 获取成功
// @Failure 403 获取失败
// @router /AddLabels/ [get]
func (b *LabelController) GetAssist() {
	assist := models.AssistObj{Blog: models.Blog{Id: 0, BlogContent: "打架", BlogShowWords: "gbhtierwoeqjfo;p1q2", BlogDate: time.Now()}, Types: []string{"呵呵", "嘿嘿"}, Labels: []string{"嘻嘻", "讷讷"}}
	b.Data["json"] = models.GetRevaOk(assist)
	b.ServeJSON()
}

// GetTypesByUid
// @Title GetTypesByUid
// @Description 获取用户所有博客的分类
// @Param uid	path	int true	"请输入查询用户的id"
// @Success 201 获取成功
// @Failure 403 获取失败
// @router /getTypesByUid/:uid [get]
func (b *LabelController) GetTypesByUid() {
	var typeNames []string
	uid, err := b.GetInt(":uid")
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取参数失败")
		b.ServeJSON()
		return
	}
	var types models.Type
	var allTypes []models.Type
	all, err := b.o.QueryTable(types.TableName()).Filter("uid_id", uid).All(&allTypes)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取参数失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		typeNames = append(typeNames, allTypes[i].TypeName)
	}
	b.Data["json"] = models.GetRevaOk(utils.StringSliceWeightRemoval(typeNames))
	b.ServeJSON()
}

// GetTypesByDomainNameSuffix
// @Title GetTypesByDomainNameSuffix
// @Description 获取用户所有博客的分类
// @Param DomainNameSuffix	path	string true	"请输入查询用户的域名后缀"
// @Success 201 获取成功
// @Failure 403 获取失败
// @router /getTypesByDomainNameSuffix/:DomainNameSuffix [get]
func (b *LabelController) GetTypesByDomainNameSuffix() {
	var typeNames []string
	domainNameSuffix := b.GetString(":DomainNameSuffix")
	var user = models.User{DomainNameSuffix: domainNameSuffix}
	err2 := b.o.Read(&user, "domain_name_suffix")
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "从数据库中获取用户信息失败")
		b.ServeJSON()
		return
	}
	var types models.Type
	var allTypes []models.Type
	all, err := b.o.QueryTable(types.TableName()).Filter("uid_id", user.Id).All(&allTypes)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取参数失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		typeNames = append(typeNames, allTypes[i].TypeName)
	}
	b.Data["json"] = models.GetRevaOk(utils.StringSliceWeightRemoval(typeNames))
	b.ServeJSON()
}

// GetLabelsByUid
// @Title GetLabelsByUid
// @Description 获取用户所有博客的标签
// @Param uid	path	int true	"请输入查询用户的id"
// @Success 201 获取成功
// @Failure 403 获取失败
// @router /getLabelsByUid/:uid [get]
func (b *LabelController) GetLabelsByUid() {
	var labelNames []string
	uid, err := b.GetInt(":uid")
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取参数失败")
		b.ServeJSON()
		return
	}
	var label models.Label
	var allLabels []models.Label
	all, err := b.o.QueryTable(label.TableName()).Filter("uid_id", uid).All(&allLabels)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取参数失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		labelNames = append(labelNames, allLabels[i].LabelName)
	}
	b.Data["json"] = models.GetRevaOk(utils.StringSliceWeightRemoval(labelNames))
	b.ServeJSON()
}

// GetLabelsByDomainNameSuffix
// @Title GetLabelsByDomainNameSuffix
// @Description 获取用户所有博客的标签
// @Param DomainNameSuffix	path	string true	"请输入查询用户的域名后缀"
// @Success 201 获取成功
// @Failure 403 获取失败
// @router /getLabelsByDomainNameSuffix/:DomainNameSuffix [get]
func (b *LabelController) GetLabelsByDomainNameSuffix() {
	var labelNames []string
	domainNameSuffix := b.GetString(":DomainNameSuffix")
	var user = models.User{DomainNameSuffix: domainNameSuffix}
	err2 := b.o.Read(&user, "domain_name_suffix")
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "从数据库中获取用户信息失败")
		b.ServeJSON()
		return
	}
	var label models.Label
	var allLabels []models.Label
	all, err := b.o.QueryTable(label.TableName()).Filter("uid_id", user.Id).All(&allLabels)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取参数失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		labelNames = append(labelNames, allLabels[i].LabelName)
	}
	b.Data["json"] = models.GetRevaOk(utils.StringSliceWeightRemoval(labelNames))
	b.ServeJSON()
}

// GetTypesAndCountByUid
// @Title GetTypesAndCountByUid
// @Description 获取用户所有博客的分类和博客数目
// @Param uid	path	int true	"请输入查询用户的id"
// @Success 201 获取成功
// @Failure 403 获取失败
// @router /getTypesAndCountByUid/:uid [get]
func (b *LabelController) GetTypesAndCountByUid() {
	uid, err := b.GetInt(":uid")
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取参数失败")
		b.ServeJSON()
		return
	}
	var types models.Type
	var allTypes []models.Type
	all, err := b.o.QueryTable(types.TableName()).Filter("uid_id", uid).All(&allTypes)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取参数失败")
		b.ServeJSON()
		return
	}
	typesAndCount := make(map[string]int64)
	for i := 0; i < int(all); i++ {
		count, err := b.o.QueryTable(types.TableName()).Filter("type_name", allTypes[i].TypeName).Filter("bid_id__gte", 0).Count()
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库获取"+allTypes[i].TypeName+"的博客数量失败")
			b.ServeJSON()
			return
		}
		typesAndCount[allTypes[i].TypeName] = count
	}
	b.Data["json"] = models.GetRevaOk(typesAndCount)
	b.ServeJSON()
}

// GetLabelsAndCountByUid
// @Title GetLabelsAndCountByUid
// @Description 获取用户所有博客的标签和博客数目
// @Param uid	path	int true	"请输入查询用户的id"
// @Success 201 获取成功
// @Failure 403 获取失败
// @router /getLabelsAndCountByUid/:uid [get]
func (b *LabelController) GetLabelsAndCountByUid() {
	uid, err := b.GetInt(":uid")
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取参数失败")
		b.ServeJSON()
		return
	}
	var labels models.Label
	var allLabels []models.Label
	all, err := b.o.QueryTable(labels.TableName()).Filter("uid_id", uid).All(&allLabels)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取参数失败")
		b.ServeJSON()
		return
	}
	labelsAndCount := make(map[string]int64)
	for i := 0; i < int(all); i++ {
		if labelsAndCount[allLabels[i].LabelName] != 0 {
			continue
		}
		count, err := b.o.QueryTable(labels.TableName()).Filter("label_name", allLabels[i].LabelName).Filter("bid_id__gte", 1).Count()
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库获取"+allLabels[i].LabelName+"的博客数量失败")
			b.ServeJSON()
			return
		}
		labelsAndCount[allLabels[i].LabelName] = count
	}
	b.Data["json"] = models.GetRevaOk(labelsAndCount)
	b.ServeJSON()
}

// DeleteLabel
// @Title DeleteLabel
// @Description 删除标签
// @Param label	path	string true	"标签"
// @Param uid	path	int    true	"用户id"
// @Success 201 删除成功
// @Failure 403 删除失败
// @router /delLabel/ [delete]
func (b *LabelController) DeleteLabel() {
	labelName := b.Input().Get("label")
	param := b.Input().Get("uid")
	uid, err2 := strconv.Atoi(param)
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "格式转换错误")
		b.ServeJSON()
		return
	}
	var label = models.Label{LabelName: labelName, Uid: &models.User{Id: uid}}

	i, err := b.o.Delete(&label, "LabelName", "uid_id")
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "删除标签失败")
		b.ServeJSON()
		return
	}

	if i == 0 {
		b.Data["json"] = models.GetRevaOk("输入的标签或用户id错误")
		b.ServeJSON()
		return
	}
	b.Data["json"] = models.GetRevaOk("删除标签成功，共移除" + strconv.Itoa(int(i)) + "个博客的此标签")
	b.ServeJSON()
}

// DeleteType
// @Title DeleteType
// @Description 删除分类
// @Param type	path	string true	"分类"
// @Param uid	path	int true	"用户id"
// @Success 201 删除成功
// @Failure 403 删除失败
// @router /delType/ [delete]
func (b *LabelController) DeleteType() {
	typeName := b.Input().Get("type")
	param := b.Input().Get("uid")
	uid, err2 := strconv.Atoi(param)
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "格式转换错误")
		b.ServeJSON()
		return
	}
	var types = models.Type{TypeName: typeName, Uid: &models.User{Id: uid}}

	i, err := b.o.Delete(&types, "TypeName", "uid_id")
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "删除分类失败")
		b.ServeJSON()
		return
	}

	if i == 0 {
		b.Data["json"] = models.GetRevaOk("输入的分类名或用户id错误")
		b.ServeJSON()
		return
	}
	b.Data["json"] = models.GetRevaOk("删除分类成功，共移除" + strconv.Itoa(int(i)) + "个博客的此分类")
	b.ServeJSON()
}

// UpdateType
// @Title UpdateType
// @Description 更新分类
// @Param type	body	models.Type true	"分类"
// @Success 201 更新成功
// @Failure 403 更新失败
// @router /updateType/ [put]
func (b *LabelController) UpdateType() {
	var types models.Type

	err := json.Unmarshal(b.Ctx.Input.RequestBody, &types)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "从JSON信息中解析到结构体失败")
		b.ServeJSON()
		return
	}

	update, err := b.o.Update(&types, "TypeName")
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "更新分类失败")
		b.ServeJSON()
		return
	}

	b.Data["json"] = models.GetRevaOk(update)
	b.ServeJSON()
}

// UpdateTypeNameByOldTypeName
// @Title UpdateTypeNameByOldTypeName
// @Description 修改分类名根据分类旧名
// @Param oldTypeName	path	string true	"分类旧名"
// @Param newTypename	path	string true	"分类新"
// @Param uid	path	int true	"用户id"
// @Success 201 更新成功
// @Failure 403 更新失败
// @router /updateTypeNameByOldTypeName/ [put]
func (b *LabelController) UpdateTypeNameByOldTypeName() {
	oldTypeName := b.Input().Get("oldTypeName")
	newTypename := b.Input().Get("newTypename")
	param := b.Input().Get("uid")
	uid, err := strconv.Atoi(param)
	if err != nil {
		logs.Error("转换格式错误")
		b.Data["json"] = models.GetRevaErr(err, "转换格式错误")
		b.ServeJSON()
		return
	}
	var types = models.Type{TypeName: oldTypeName, Uid: &models.User{Id: uid}}

	err = b.o.Read(&types, "TypeName", "Uid")
	if err != nil {
		logs.Error("获取分类失败")
		b.Data["json"] = models.GetRevaErr(err, "获取分类失败")
		b.ServeJSON()
		return
	}

	types.TypeName = newTypename
	update, err := b.o.Update(&types)
	if err != nil {
		logs.Error("更改分类名失败")
		b.Data["json"] = models.GetRevaErr(err, "更改分类名失败")
		b.ServeJSON()
		return
	}

	b.Data["json"] = models.GetRevaOk(update)
	b.ServeJSON()
}

// UpdateLabel
// @Title UpdateLabel
// @Description 更新标签
// @Param label	body	models.Label true	"标签"
// @Success 201 更新成功
// @Failure 403 更新失败
// @router /updateLabel/ [put]
func (b *LabelController) UpdateLabel() {
	var label models.Label

	err := json.Unmarshal(b.Ctx.Input.RequestBody, &label)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "从JSON信息中解析到结构体失败")
		b.ServeJSON()
		return
	}

	update, err := b.o.Update(&label, "LabelName")
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "更新标签失败")
		b.ServeJSON()
		return
	}

	b.Data["json"] = models.GetRevaOk("更新标签成功，共更新" + strconv.Itoa(int(update)) + "个博客的此标签")
	b.ServeJSON()
}

// UpdateLabelByOldLabelName
// @Title UpdateLabelByOldLabelName
// @Description 修改标签名根据标签旧名
// @Param oldLabelName	path	string true	"标签旧名"
// @Param newLabelName	path	string true	"标签新名"
// @Param uid	path	int true	"用户id"
// @Success 201 更新成功
// @Failure 403 更新失败
// @router /updateLabelByOldLabelName/ [put]
func (b *LabelController) UpdateLabelByOldLabelName() {
	oldLabelName := b.Input().Get("oldLabelName")
	newLabelName := b.Input().Get("newLabelName")
	param := b.Input().Get("uid")
	uid, err := strconv.Atoi(param)
	if err != nil {
		logs.Error("转换格式错误")
		b.Data["json"] = models.GetRevaErr(err, "转换格式错误")
		b.ServeJSON()
		return
	}
	var labels = models.Label{LabelName: oldLabelName, Uid: &models.User{Id: uid}}

	err = b.o.Read(&labels, "LabelName", "Uid")
	if err != nil {
		logs.Error("获取分类失败")
		b.Data["json"] = models.GetRevaErr(err, "获取分类失败")
		b.ServeJSON()
		return
	}

	labels.LabelName = newLabelName
	update, err := b.o.Update(&labels)
	if err != nil {
		logs.Error("更改分类名失败")
		b.Data["json"] = models.GetRevaErr(err, "更改分类名失败")
		b.ServeJSON()
		return
	}

	b.Data["json"] = models.GetRevaOk(update)
	b.ServeJSON()
}
