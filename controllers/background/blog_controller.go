package controllers

import (
	"bannerstudioblog/models"
	"bannerstudioblog/utils"
	"encoding/json"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/gomodule/redigo/redis"
	"strconv"
	"time"
)

type BlogController struct {
	baseController
}

// GetBlog
// @Title	GetBlog
// @Description Query a blog
// @Param	id 	path	int true	"Query a blog by ID"
// @Success 201 Query a blog successfully
// @Failure 403 Failed to query a blog
// @router /GetBlog/:id [get]
func (b *BlogController) GetBlog() {
	Id, err := b.GetInt(":id")
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
		b.ServeJSON()
		return
	}
	var blog = models.Blog{Id: Id}
	err = b.o.Read(&blog)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		b.ServeJSON()
		return
	}
	var types models.Type
	var allType []models.Type
	allTypes, err := b.o.QueryTable(types.TableName()).Filter("bid_id", blog.Id).All(&allType)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客分类失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(allTypes); i++ {
		blog.Types = append(blog.Types, &allType[i])
	}
	var labels models.Label
	var allLabel []models.Label
	allLabels, err := b.o.QueryTable(labels.TableName()).Filter("bid_id", blog.Id).All(&allLabel)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客标签失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(allLabels); i++ {
		blog.Labels = append(blog.Labels, &allLabel[i])
	}
	// 获取博客浏览量
	views, err := blog.GetViews(Id)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取博客浏览量失败")
		b.ServeJSON()
		return
	}
	b.Data["json"] = models.GetRevaOkAndDes(struct {
		Blog models.Blog
		read int
	}{
		blog,
		views,
	}, "该博客有"+strconv.Itoa(int(allLabels))+"条标签和"+strconv.Itoa(int(allTypes))+"种分类")
	b.ServeJSON()
}

// AddBlog
// @Title AddBlog
// @Description 添加博客信息
// @Param 辅助结构体	body	assistObj true	"请输入blog结构体的json格式"
// @Success 201 添加成功
// @Failure 403 添加失败
// @router /AddBlog/ [post]
func (b *BlogController) AddBlog() {
	var assistObj models.AssistObj
	err2 := json.Unmarshal(b.Ctx.Input.RequestBody, &assistObj)
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "从请求中获取结构体失败")
		b.ServeJSON()
		return
	}
	blog := assistObj.Blog
	blog.BlogDate = time.Now()
	if len([]byte(utils.TrimHtml(utils.Markdown2Html(blog.BlogContent)))) < 255 {
		blog.BlogShowWords = utils.TrimHtml(utils.Markdown2Html(blog.BlogContent))
	} else {
		blog.BlogShowWords = string([]byte(utils.TrimHtml(utils.Markdown2Html(blog.BlogContent)))[0:255])
	}
	id, err := b.o.Insert(&blog)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "插入博客时后台操作数据库失败")
		b.ServeJSON()
		return
	}
	assistObj.Blog.Id = int(id)
	typeNames := assistObj.Types
	for _, name := range typeNames {
		types := models.Type{Bid: &models.Blog{Id: int(id)}, TypeName: name, Uid: &models.User{Id: blog.Uid.Id}}
		_, err3 := b.o.Insert(&types)
		if err3 != nil {
			b.Data["json"] = models.GetRevaErr(err3, "插入分类时后台操作数据库失败")
			b.ServeJSON()
			return
		}
	}
	labelsName := assistObj.Labels
	for _, name := range labelsName {
		labels := models.Label{Bid: &models.Blog{Id: int(id)}, LabelName: name, Uid: &models.User{Id: blog.Uid.Id}}
		_, err3 := b.o.Insert(&labels)
		if err3 != nil {
			b.Data["json"] = models.GetRevaErr(err3, "插入标签时后台操作数据库失败")
			b.ServeJSON()
			return
		}
	}
	b.Data["json"] = models.GetRevaOk(assistObj)
	b.ServeJSON()
}

// DeleteBlog
// @Title DeleteBlog
// @Description	Delete the blog
// @Param id path	int	true "Delete blog by ID"
// @Success 201	Succeeded in deleting the blog by ID
// @Failure 403 Failed to delete blog by ID
// @router /DeleteBlog/:id [delete]
func (b *BlogController) DeleteBlog() {
	Id, err := b.GetInt(":id")
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
		b.ServeJSON()
		return
	}
	i, err := b.o.Delete(&models.Blog{Id: Id})
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		b.ServeJSON()
		return
	}
	b.Data["json"] = models.GetRevaOk("成功删除" + strconv.Itoa(int(i)) + "条数据")
	b.ServeJSON()
}

// UpdateBlog
// @Title UpdateBlog
// @Description Update the blog
// @Param body body		assistObj	true "Update the blog"
// @Success 201	Update blog successfully
// @Failure 403 Failed to update blog
// @router /UpdateBlog/ [put]
func (b *BlogController) UpdateBlog() {
	var assist models.AssistObj
	if err2 := json.Unmarshal(b.Ctx.Input.RequestBody, &assist); err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "从RequestBody信息映射到结构体中失败")
		b.ServeJSON()
		return
	}

	var blog = models.Blog{Id: assist.Blog.Id}
	err2 := b.o.Read(&blog)
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "后台操作数据库失败")
		b.ServeJSON()
		return
	}

	assist.Blog.BlogDate = blog.BlogDate
	if len(utils.TrimHtml(utils.Markdown2Html(assist.Blog.BlogContent))) < 255 {
		blog.BlogShowWords = utils.TrimHtml(utils.Markdown2Html(assist.Blog.BlogContent))
	} else {
		blog.BlogShowWords = utils.TrimHtml(utils.Markdown2Html(assist.Blog.BlogContent))[0:255]
	}
	_, err := b.o.Update(&assist.Blog)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		b.ServeJSON()
		return
	}

	err = b.o.Begin()
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "开启事务失败")
		b.ServeJSON()
		return
	}
	var types = models.Type{Uid: &models.User{Id: assist.Blog.Uid.Id}, Bid: &models.Blog{Id: assist.Blog.Id}}
	_, err = b.o.Delete(&types, "uid_id", "bid_id")
	if err != nil {
		err2 := b.o.Rollback()
		if err2 != nil {
			logs.Error("事务回滚失败")
		}
		b.Data["json"] = models.GetRevaErr(err, "删除原本分类失败")
		b.ServeJSON()
		return
	}
	for _, s := range assist.Types {
		var allTypes = models.Type{TypeName: s, Uid: &models.User{Id: assist.Blog.Uid.Id}, Bid: &models.Blog{Id: assist.Blog.Id}}
		_, err2 := b.o.Insert(&allTypes)
		if err2 != nil {
			if b.o.Rollback() != nil {
				logs.Error("事务回滚失败")
			}
			b.Data["json"] = models.GetRevaErr(err2, "插入分类失败")
			b.ServeJSON()
			return
		}
	}
	err = b.o.Commit()
	if err != nil {
		logs.Error("提交事务失败")
		b.Data["json"] = models.GetRevaErr(err, "提交事务失败")
		b.ServeJSON()
		return
	}

	err = b.o.Begin()
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "开启事务失败")
		b.ServeJSON()
		return
	}
	var labels = models.Label{Uid: &models.User{Id: assist.Blog.Uid.Id}, Bid: &models.Blog{Id: assist.Blog.Id}}
	_, err = b.o.Delete(&labels, "uid_id", "bid_id")
	if err != nil {
		err2 := b.o.Rollback()
		if err2 != nil {
			logs.Error("事务回滚失败")
		}
		b.Data["json"] = models.GetRevaErr(err, "删除原本标签失败")
		b.ServeJSON()
		return
	}
	for _, s := range assist.Labels {
		var allLabels = models.Label{LabelName: s, Uid: &models.User{Id: assist.Blog.Uid.Id}, Bid: &models.Blog{Id: assist.Blog.Id}}
		_, err2 := b.o.Insert(&allLabels)
		if err2 != nil {
			if b.o.Rollback() != nil {
				logs.Error("事务回滚失败")
			}
			b.Data["json"] = models.GetRevaErr(err2, "插入分类失败")
			b.ServeJSON()
			return
		}
	}
	err = b.o.Commit()
	if err != nil {
		logs.Error("提交事务失败")
		b.Data["json"] = models.GetRevaErr(err, "提交事务失败")
		b.ServeJSON()
		return
	}
	b.Data["json"] = models.GetRevaOk(assist)
	b.ServeJSON()
}

// GetAllBlog
// @Title	GetAllBlog
// @Description Query all blogs
// @Success 201 Query all blogs successfully
// @Failure 403 Failed to query all blogs
// @router /getAllBlog/ [get]
func (b *BlogController) GetAllBlog() {
	var blogs []models.Blog
	blog := models.Blog{}
	all, err := b.o.QueryTable(blog.TableName()).All(&blogs)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取全部内容失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		var types models.Type
		var allType []models.Type
		allTypes, err := b.o.QueryTable(types.TableName()).Filter("bid_id", blogs[i].Id).All(&allType)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客分类失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allTypes); j++ {
			blogs[i].Types = append(blogs[i].Types, &allType[j])
		}
		var labels models.Label
		var allLabel []models.Label
		allLabels, err := b.o.QueryTable(labels.TableName()).Filter("bid_id", blogs[i].Id).All(&allLabel)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客标签失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allLabels); j++ {
			blogs[i].Labels = append(blogs[i].Labels, &allLabel[j])
		}
	}
	b.Data["json"] = models.GetRevaOk(blogs)
	b.ServeJSON()
}

// @Title	GetAllArticle
// @Description Query all articles
// @Success 201 Query all articles successfully
// @Failure 403 Failed to query all articles
// @router /getAllArticle/ [get]
func (b *BlogController) GetAllArticle() {
	var blogs []models.Blog
	blog := models.Blog{}
	all, err := b.o.QueryTable(blog.TableName()).Filter("blog_state", 0).All(&blogs)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取全部文章失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		var types models.Type
		var allType []models.Type
		allTypes, err := b.o.QueryTable(types.TableName()).Filter("bid_id", blogs[i].Id).All(&allType)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客分类失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allTypes); j++ {
			blogs[i].Types = append(blogs[i].Types, &allType[j])
		}
		var labels models.Label
		var allLabel []models.Label
		allLabels, err := b.o.QueryTable(labels.TableName()).Filter("bid_id", blogs[i].Id).All(&allLabel)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客标签失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allLabels); j++ {
			blogs[i].Labels = append(blogs[i].Labels, &allLabel[j])
		}
	}
	b.Data["json"] = models.GetRevaOk(blogs)
	b.ServeJSON()
}

// GetAllDraft
// @Title	GetAllDraft
// @Description Query all drafts
// @Success 201 Query all drafts successfully
// @Failure 403 Failed to query all drafts
// @router /getAllDraft/ [get]
func (b *BlogController) GetAllDraft() {
	var blogs []models.Blog
	blog := models.Blog{}
	all, err := b.o.QueryTable(blog.TableName()).Filter("blog_state", 1).All(&blogs)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取全部草稿失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		var types models.Type
		var allType []models.Type
		allTypes, err := b.o.QueryTable(types.TableName()).Filter("bid_id", blogs[i].Id).All(&allType)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客分类失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allTypes); j++ {
			blogs[i].Types = append(blogs[i].Types, &allType[j])
		}
		var labels models.Label
		var allLabel []models.Label
		allLabels, err := b.o.QueryTable(labels.TableName()).Filter("bid_id", blogs[i].Id).All(&allLabel)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客标签失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allLabels); j++ {
			blogs[i].Labels = append(blogs[i].Labels, &allLabel[j])
		}
	}
	b.Data["json"] = models.GetRevaOk(blogs)
	b.ServeJSON()
}

// GetAllBlogByUid
// @Title	GetAllBlogByUid
// @Description 根据uid查询所有博客
// @Param	uid 	path	int true	"根据uid查询所有博客"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /GetAllBlogByUid/:uid [get]
func (b *BlogController) GetAllBlogByUid() {
	uid, err2 := b.GetInt(":uid")
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "后台获取数据失败")
		b.ServeJSON()
		return
	}
	var blogs []models.Blog
	blog := models.Blog{}
	all, err := b.o.QueryTable(blog.TableName()).Filter("uid", uid).All(&blogs)
	if err != nil {
		beego.Error(err)
		b.Data["json"] = models.GetRevaErr(err, "获取全部内容失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		var types models.Type
		var allType []models.Type
		allTypes, err := b.o.QueryTable(types.TableName()).Filter("bid_id", blogs[i].Id).All(&allType)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客分类失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allTypes); j++ {
			blogs[i].Types = append(blogs[i].Types, &allType[j])
		}
		var labels models.Label
		var allLabel []models.Label
		allLabels, err := b.o.QueryTable(labels.TableName()).Filter("bid_id", blogs[i].Id).All(&allLabel)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客标签失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allLabels); j++ {
			blogs[i].Labels = append(blogs[i].Labels, &allLabel[j])
		}
	}
	b.Data["json"] = models.GetRevaOk(blogs)
	b.ServeJSON()
}

// GetAllArticleByUid
// @Title	GetAllArticleByUid
// @Description 根据uid查询所用文章
// @Param	uid 	path	int true	"根据uid查询所有文章"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /GetAllArticleByUid/:uid [get]
func (b *BlogController) GetAllArticleByUid() {
	uid, err2 := b.GetInt(":uid")
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "后台获取数据失败")
		b.ServeJSON()
		return
	}
	var blogs []models.Blog
	blog := models.Blog{}
	all, err := b.o.QueryTable(blog.TableName()).Filter("blog_state", 0).Filter("uid", uid).All(&blogs)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取全部文章失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		var types models.Type
		var allType []models.Type
		allTypes, err := b.o.QueryTable(types.TableName()).Filter("bid_id", blogs[i].Id).All(&allType)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客分类失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allTypes); j++ {
			blogs[i].Types = append(blogs[i].Types, &allType[j])
		}
		var labels models.Label
		var allLabel []models.Label
		allLabels, err := b.o.QueryTable(labels.TableName()).Filter("bid_id", blogs[i].Id).All(&allLabel)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客标签失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allLabels); j++ {
			blogs[i].Labels = append(blogs[i].Labels, &allLabel[j])
		}
	}
	b.Data["json"] = models.GetRevaOk(blogs)
	b.ServeJSON()
}

// GetAllArticleByDomainNameSuffix
// @Title	GetAllArticleByDomainNameSuffix
// @Description 根据域名后缀查询所有文章
// @Param	domainNameSuffix 	path	string true	"根据域名后缀查询所有文章"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /GetAllArticleByDomainNameSuffix/:domainNameSuffix [get]
func (b *BlogController) GetAllArticleByDomainNameSuffix() {
	domainNameSuffix := b.GetString(":domainNameSuffix")
	var blogs []models.Blog
	userinfo := models.User{DomainNameSuffix: domainNameSuffix}
	err2 := b.o.Read(&userinfo, "DomainNameSuffix")
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "后台获取数据失败")
		b.ServeJSON()
		return
	}
	blog := models.Blog{}
	all, err := b.o.QueryTable(blog.TableName()).Filter("uid", userinfo.Id).Filter("blog_state", 0).All(&blogs)
	if err != nil {
		beego.Error(err)
		b.Data["json"] = models.GetRevaErr(err, "获取全部文章失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		var types models.Type
		var allType []models.Type
		allTypes, err := b.o.QueryTable(types.TableName()).Filter("bid_id", blogs[i].Id).All(&allType)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客分类失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allTypes); j++ {
			blogs[i].Types = append(blogs[i].Types, &allType[j])
		}
		var labels models.Label
		var allLabel []models.Label
		allLabels, err := b.o.QueryTable(labels.TableName()).Filter("bid_id", blogs[i].Id).All(&allLabel)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客标签失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allLabels); j++ {
			blogs[i].Labels = append(blogs[i].Labels, &allLabel[j])
		}
	}
	b.Data["json"] = models.GetRevaOk(blogs)
	b.ServeJSON()
}

// GetAllDraftByUid
// @Title	GetAllDraftByUid
// @Description 根据uid查询所有草稿
// @Param	uid 	path	int true	"根据uid查询所有草稿"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /GetAllDraftByUid/:uid [get]
func (b *BlogController) GetAllDraftByUid() {
	uid, err2 := b.GetInt(":uid")
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "后台获取数据失败")
		b.ServeJSON()
		return
	}
	var blogs []models.Blog
	blog := models.Blog{}
	all, err := b.o.QueryTable(blog.TableName()).Filter("uid", uid).Filter("blog_state", 1).All(&blogs)
	if err != nil {
		beego.Error(err)
		b.Data["json"] = models.GetRevaErr(err, "获取全部草稿失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		var types models.Type
		var allType []models.Type
		allTypes, err := b.o.QueryTable(types.TableName()).Filter("bid_id", blogs[i].Id).All(&allType)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客分类失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allTypes); j++ {
			blogs[i].Types = append(blogs[i].Types, &allType[j])
		}
		var labels models.Label
		var allLabel []models.Label
		allLabels, err := b.o.QueryTable(labels.TableName()).Filter("bid_id", blogs[i].Id).All(&allLabel)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "从数据库中查询博客标签失败")
			b.ServeJSON()
			return
		}
		for j := 0; j < int(allLabels); j++ {
			blogs[i].Labels = append(blogs[i].Labels, &allLabel[j])
		}
	}
	b.Data["json"] = models.GetRevaOk(blogs)
	b.ServeJSON()
}

//// GetBlogByLabel
//// @Title GetBlogByLabel
//// @Description Articles obtained by labels
//// @Param	type 	path	string	true	"请填写文章标签"
//// @Param	uid 	path	int		true	"请填写用户id"
//// @Success 201 Success of articles obtained through tags
//// @Failure 403 The article obtained by the label failed
//// @router /getBlogByLabel/:uid/:label [get]
//func (b *BlogController) GetBlogByLabel() {
//	labelName := b.GetString(":label")
//	uid, err2 := b.GetInt(":uid")
//	if err2 != nil {
//		b.Data["json"] = models.GetRevaErr(err2, "后台获取数据失败")
//		b.ServeJSON()
//		return
//	}
//	var blogs []models.Blog
//	var labels []models.Label
//	label := models.Label{}
//	_, err := b.o.QueryTable(label.TableName()).Filter("label_name", labelName).All(&labels)
//	if err != nil {
//		beego.Error(err)
//		b.Data["json"] = models.GetRevaErr(err, "获取标签结构体失败")
//		b.ServeJSON()
//		return
//	}
//	for _, m := range labels {
//		blog := models.Blog{Id: m.Bid.Id}
//		b.o.Read(&blog)
//		if blog.Uid.Id == uid {
//			blogs = append(blogs, blog)
//		}
//	}
//	//_, err := b.o.QueryTable(blog.TableName()).Filter("blog_state", 0).Filter("uid",id).All(&blogs)
//	//if err != nil {
//	//	beego.Error(err)
//	//	b.Data["json"] = models.GetRevaErr(err, "根据标签获取全部文章失败")
//	//	b.ServeJSON()
//	//}
//	b.Data["json"] = models.GetRevaOk(blogs)
//	b.ServeJSON()
//}

//// GetBlogByType
//// @Title GetBlogByType
//// @Description Articles obtained by type
//// @Param	type 	path	string 	true	"请填写文章分类"
//// @Param	uid 	path	int		true	"请填写用户id"
//// @Success 201 Success of articles obtained through type
//// @Failure 403 The article obtained by the type failed
////// @router /getBlogByType/:uid/:type [get]
//func (b *BlogController) GetBlogByType() {
//	typeName := b.GetString(":type")
//	uid, err2 := b.GetInt(":uid")
//	if err2 != nil {
//		b.Data["json"] = models.GetRevaErr(err2, "后台获取数据失败")
//		b.ServeJSON()
//		return
//	}
//	var blogs []models.Blog
//	var labels []models.Label
//	label := models.Label{}
//	_, err := b.o.QueryTable(label.TableName()).Filter("type_name", typeName).All(&labels)
//	if err != nil {
//		b.Data["json"] = models.GetRevaErr(err, "获取标签结构体失败")
//		b.ServeJSON()
//		return
//	}
//	for _, m := range labels {
//		blog := models.Blog{Id: m.Bid.Id}
//		b.o.Read(&blog)
//		if blog.Uid.Id == uid {
//			blogs = append(blogs, blog)
//		}
//	}
//	b.Data["json"] = models.GetRevaOk(blogs)
//	b.ServeJSON()
//}

// GetAllBlogViews
// @Title	GetAllBlogViews
// @Description Query all blogViews
// @Success 201 Query all blogViews successfully
// @Failure 403 Failed to query all blogViews
// @router /getAllBlogViews/ [get]
func (b *BlogController) GetAllBlogViews() {
	var bolg models.Blog
	views, err := bolg.GetAllViews()
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取博客总浏览量失败")
		b.ServeJSON()
		return
	}
	b.Data["json"] = models.GetRevaOk(views)
	b.ServeJSON()
}

// GetBlogViews
// @Title	GetBlogViews
// @Description Query a blogView
// @Param	id 	path	int true	"Query a blogView by ID"
// @Success 201 Query a blogView successfully
// @Failure 403 Failed to query a blogView
// @router /getBlogViews/:id [get]
func (b *BlogController) GetBlogViews() {
	Id, err := b.GetInt(":id")
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取参数失败")
		b.ServeJSON()
		return
	}
	var blog models.Blog
	views, err := blog.GetViews(Id)
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取bolg浏览量失败")
		b.ServeJSON()
		return
	}
	b.Data["json"] = models.GetRevaOk(views)
	b.ServeJSON()
}

// GetViewsInWeek
// @Title	GetViewsInWeek
// @Description Query the number of page views in a week
// @Success 201 Query the number of page views for a week succeeded
// @Failure 403 Failed to query weekly page views
// @router /GetViewsInWeek/ [get]
func (b *BlogController) GetViewsInWeek() {
	m := make(map[string]int)
	sum := 0
	for i := 0; i < 7; i++ {
		viewsInDay := "blogViews:" + time.Unix(time.Now().Unix()-int64(i*86400), 0).String()[:10]
		do, err := b.pool.Do("EXISTS", viewsInDay)
		if err != nil {
			b.Data["json"] = models.GetRevaErr(err, "链接redis数据库失败")
			b.ServeJSON()
			return
		}
		if do == int64(1) {
			num, err := redis.Int(b.pool.Do("get", viewsInDay))
			if err != nil {
				b.Data["json"] = models.GetRevaErr(err, "非关系型数据库转换类型失败")
				b.ServeJSON()
				return
			}
			m[time.Unix(time.Now().Unix()-int64(i*86400), 0).String()[:10]] = num
			sum += num
		} else {
			m[time.Unix(time.Now().Unix()-int64(i*86400), 0).String()[:10]] = 0
		}
	}
	m["sum"] = sum
	b.Data["json"] = models.GetRevaOk(m)
	b.ServeJSON()
}

// Test
// @Title	GetViewsInWeek
// @Description Query the number of page views in a week
// @Success 201 Query the number of page views for a week succeeded
// @Failure 403 Failed to query weekly page views
// @router /Test/ [get]
//func (t *BlogController) Test() {
//	obj := models.AssistObj{1, []string{"123", "123"}, []string{}}
//	beego.Error(obj)
//	t.Data["json"] = obj
//	t.ServeJSON()
//}

// GetBlogByType
// @Title GetBlogByType
// @Description Articles obtained by type
// @Param	type 	path	string 	true	"请填写文章分类"
// @Param	uid 	path	int		true	"请填写用户id"
// @Success 201 根据分类查博客信息成功
// @Failure 403 根据分类查博客信息失败
// @router /getBlogByType/:uid/:type [get]
func (b *BlogController) GetBlogByType() {
	typeName := b.GetString(":type")
	uid, err2 := b.GetInt(":uid")
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "后台获取参数失败")
		b.ServeJSON()
		return
	}
	var blogs []models.Blog
	var types models.Type
	var allTypes []models.Type
	all, err2 := b.o.QueryTable(types.TableName()).Filter("type_name", typeName).Filter("uid_id", uid).All(&allTypes)
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "从数据库获取类型数组失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		var blog = models.Blog{Id: allTypes[i].Bid.Id}
		err2 := b.o.Read(&blog)
		if err2 != nil {
			b.Data["json"] = models.GetRevaErr(err2, "从数据库获取博客失败")
			b.ServeJSON()
			return
		}
		blogs = append(blogs, blog)
	}
	b.Data["json"] = models.GetRevaOk(blogs)
	b.ServeJSON()
}

// GetBlogByLabel
// @Title GetBlogByLabel
// @Description Articles obtained by label
// @Param	label 	path	string 	true	"请填写文章标签"
// @Param	uid 	path	int		true	"请填写用户id"
// @Success 201 根据分类查博客信息成功
// @Failure 403 根据分类查博客信息失败
// @router /getBlogByLabel/:uid/:label [get]
func (b *BlogController) GetBlogByLabel() {
	label := b.GetString(":label")
	uid, err2 := b.GetInt(":uid")
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "后台获取参数失败")
		b.ServeJSON()
		return
	}
	var blogs []models.Blog
	var labels models.Label
	var allLabels []models.Label
	all, err2 := b.o.QueryTable(labels.TableName()).Filter("label_name", label).Filter("uid_id", uid).All(&allLabels)
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "从数据库获取标签数组失败")
		b.ServeJSON()
		return
	}
	for i := 0; i < int(all); i++ {
		var blog = models.Blog{Id: allLabels[i].Bid.Id}
		err2 := b.o.Read(&blog)
		if err2 != nil {
			b.Data["json"] = models.GetRevaErr(err2, "从数据库获取博客失败")
			b.ServeJSON()
			return
		}
		blogs = append(blogs, blog)
	}
	b.Data["json"] = models.GetRevaOk(blogs)
	b.ServeJSON()
}

// GetBlogsCount
// @Title GetBlogsCount
// @Description 获取博客、草稿和文章数目
// @Success 201 根据分类查博客信息成功
// @Failure 403 根据分类查博客信息失败
// @router /getBlogsCount/ [get]
func (b *BlogController) GetBlogsCount() {
	var blog models.Blog
	var countMap = make(map[string]int)

	articleCount, err := b.o.QueryTable(blog.TableName()).Filter("blog_state", 1).Count()
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取文章数目失败")
		b.ServeJSON()
		return
	}
	draftCount, err := b.o.QueryTable(blog.TableName()).Filter("blog_state", 0).Count()
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取草稿数目失败")
		b.ServeJSON()
		return
	}

	countMap["blog"] = int(articleCount + draftCount)
	countMap["article"] = int(articleCount)
	countMap["draft"] = int(draftCount)
	b.Data["json"] = models.GetRevaOk(countMap)
	b.ServeJSON()
}

// GetBlogsCountByUid
// @Title GetBlogsCountByUid
// @Description 获取用户的博客、草稿和文章数目
// @Param	uid 	path	int true	"uid"
// @Success 201 根据用户id查博客数量成功
// @Failure 403 根据分类查博客信息失败
// @router /getBlogsCountByUid/:uid [get]
func (b *BlogController) GetBlogsCountByUid() {
	uid, err2 := b.GetInt(":uid")
	if err2 != nil {
		b.Data["json"] = models.GetRevaErr(err2, "获取路由参数失败")
		b.ServeJSON()
		return
	}
	var blog models.Blog
	var countMap = make(map[string]int)

	articleCount, err := b.o.QueryTable(blog.TableName()).Filter("blog_state", 1).Filter("uid_id", uid).Count()
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取文章数目失败")
		b.ServeJSON()
		return
	}
	draftCount, err := b.o.QueryTable(blog.TableName()).Filter("blog_state", 0).Filter("uid_id", uid).Count()
	if err != nil {
		b.Data["json"] = models.GetRevaErr(err, "获取草稿数目失败")
		b.ServeJSON()
		return
	}

	countMap["blog"] = int(articleCount + draftCount)
	countMap["article"] = int(articleCount)
	countMap["draft"] = int(draftCount)
	b.Data["json"] = models.GetRevaOk(countMap)
	b.ServeJSON()
}
