package controllers

import (
	"bannerstudioblog/models"
	"encoding/json"
	"strconv"
	"time"
)

type LeaveWordController struct {
	baseController
}

//// GetLeaveWordById
//// @Title	GetLeaveWordById
//// @Description 查看根据id查看留言
//// @Param	id 	path	int true	"请输入需要查看的留言id"
//// @Success 201 查询成功
//// @Failure 403 查询失败
//// @router /getLeaveWordById/:id [get]
//func (c *LeaveWordController) GetLeaveWordById() {
//	id, err := c.GetInt(":id")
//	if err != nil {
//		c.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
//		c.ServeJSON()
//		return
//	}
//	leaveWord := models.LeaveWord{Id: id}
//	if err := c.o.Read(&leaveWord); err != nil {
//		c.Data["json"] = models.GetRevaErr(err, "从数据库获取数据失败")
//		c.ServeJSON()
//		return
//	}
//	c.Data["json"] = models.GetRevaOk(leaveWord)
//	c.ServeJSON()
//}
//
//// GetAllLeaveWord
//// @Title	GetAllLeaveWord
//// @Description 查看所有留言
//// @Success 201 查询成功
//// @Failure 403 查询失败
//// @router /getAllLeaveWord/ [get]
//func (c *LeaveWordController) GetAllLeaveWord() {
//	var leaveWord models.LeaveWord
//	var leaveWords []models.LeaveWord
//	_, err := c.o.QueryTable(leaveWord.TableName()).All(&leaveWords)
//	if err != nil {
//		c.Data["json"] = models.GetRevaErr(err, "从数据库中获取信息失败")
//		c.ServeJSON()
//		return
//	}
//	c.Data["json"] = models.GetRevaOk(leaveWords)
//	c.ServeJSON()
//}
//
//// GetLeaveWordTwoByLId
//// @Title	GetLeaveWordTwoByLId
//// @Description 查看根据留言id查看二级留言
//// @Param	id 	path	int true	"请输入需要查看的留言id"
//// @Success 201 查询成功
//// @Failure 403 查询失败
//// @router /getLeaveWordTwoByLId/:id [get]
//func (c *LeaveWordController) GetLeaveWordTwoByLId() {
//	id, err := c.GetInt(":id")
//	if err != nil {
//		c.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
//		c.ServeJSON()
//		return
//	}
//	var leaveWordTwo models.LeaveWordTwo
//	var leaveWordTwos []models.LeaveWordTwo
//	all, err := c.o.QueryTable(leaveWordTwo.TableName()).Filter("leave_word_id", id).All(&leaveWordTwos)
//	if err != nil {
//		c.Data["json"] = models.GetRevaErr(err, "数据库获取信息失败")
//		c.ServeJSON()
//		return
//	}
//	c.Data["json"] = models.GetRevaOkAndDes(leaveWordTwos, "共有"+strconv.Itoa(int(all))+"条留言")
//	c.ServeJSON()
//}
//
//// GetAllLeaveWordTwo
//// @Title	GetAllLeaveWordTwo
//// @Description 查看所有二级留言
//// @Success 201 查询成功
//// @Failure 403 查询失败
//// @router /getAllLeaveWordTwo/ [get]
//func (c *LeaveWordController) GetAllLeaveWordTwo() {
//	var leaveWordTwo models.LeaveWordTwo
//	var leaveWordTwos []models.LeaveWordTwo
//	_, err := c.o.QueryTable(leaveWordTwo.TableName()).All(&leaveWordTwos)
//	if err != nil {
//		c.Data["json"] = models.GetRevaErr(err, "从数据库中获取信息失败")
//		c.ServeJSON()
//		return
//	}
//	c.Data["json"] = models.GetRevaOk(leaveWordTwos)
//	c.ServeJSON()
//}

// AddLeaveWord
// @Title	AddLeaveWord
// @Description 上传留言
// @Param	leaveWord 	body	models.LeaveWord true	"请输入留言的json格式"
// @Success 201 插入成功
// @Failure 403 插入失败
// @router /addLeaveWord/ [post]
func (c *LeaveWordController) AddLeaveWord() {
	var leaveWord models.LeaveWord
	err := json.Unmarshal(c.Ctx.Input.RequestBody, &leaveWord)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "从结构体中获取信息失败")
		c.ServeJSON()
		return
	}
	leaveWord.MassageTime = time.Now()
	id, err := c.o.Insert(&leaveWord)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "插入信息失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOk(id)
	c.ServeJSON()
}

// AddLeaveWordTwo
// @Title	AddLeaveWordTwo
// @Description 上传二级留言
// @Param	leaveWord 	body	models.LeaveWordTwo true	"请输入二级留言的json格式"
// @Success 201 插入成功
// @Failure 403 插入失败
// @router /addLeaveWordTwo/ [post]
func (c *LeaveWordController) AddLeaveWordTwo() {
	var leaveWordTwo models.LeaveWordTwo
	err := json.Unmarshal(c.Ctx.Input.RequestBody, &leaveWordTwo)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "从结构体中获取信息失败")
		c.ServeJSON()
		return
	}
	if leaveWordTwo.ReplyTo == "" {
		var leaveWord = models.LeaveWord{Id: leaveWordTwo.LeaveWordId.Id}
		err = c.o.Read(&leaveWord)
		if err != nil {
			c.Data["json"] = models.GetRevaErr(err, "获取留言访客信息失败")
			c.ServeJSON()
			return
		}
		var v = models.Visitor{Id: leaveWord.Vid.Id}
		err = c.o.Read(&v)
		if err != nil {
			c.Data["json"] = models.GetRevaErr(err, "获取留言访客信息失败")
			c.ServeJSON()
			return
		}
		leaveWordTwo.ReplyTo = v.VName
	}
	leaveWordTwo.MassageTime = time.Now()
	id, err := c.o.Insert(&leaveWordTwo)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "插入信息失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOk(id)
	c.ServeJSON()
}

// DeleteLeaveWordById
// @Title	DeleteLeaveWordById
// @Description 删除留言信息根据id
// @Param	id 	path	int true	"输入要删除留言的id"
// @Success 201 删除成功
// @Failure 403 删除失败
// @router /deleteLeaveWordById/:id [delete]
func (c *LeaveWordController) DeleteLeaveWordById() {
	id, err2 := c.GetInt(":id")
	if err2 != nil {
		c.Data["json"] = models.GetRevaErr(err2, "后台获取数据失败")
		c.ServeJSON()
		return
	}
	leaveWord := models.LeaveWord{Id: id}
	data, err := c.o.Delete(&leaveWord)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "删除信息失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOk(data)
	c.ServeJSON()
}

// DeleteLeaveWordTwoById
// @Title	DeleteLeaveWordTwoById
// @Description 删除二级留言信息根据id
// @Param	id 	path	int true	"输入要删除二级留言的id"
// @Success 201 删除成功
// @Failure 403 删除失败
// @router /deleteLeaveWordTwoById/:id [delete]
func (c *LeaveWordController) DeleteLeaveWordTwoById() {
	id, err2 := c.GetInt(":id")
	if err2 != nil {
		c.Data["json"] = models.GetRevaErr(err2, "后台获取数据失败")
		c.ServeJSON()
		return
	}
	leaveWord := models.LeaveWordTwo{Id: id}
	data, err := c.o.Delete(&leaveWord)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "删除信息失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOk("成功删除" + strconv.Itoa(int(data)) + "条数据")
	c.ServeJSON()
}

// GetLeaveWordByUid
// @Title	GetLeaveWordByUid
// @Description 查看根据用户id查看留言
// @Param	uid 	path	int true	"用户id"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getLeaveWordByUid/:uid [get]
func (c *LeaveWordController) GetLeaveWordByUid() {
	uid, err := c.GetInt(":uid")

	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
		c.ServeJSON()
		return
	}

	var leaveWord models.LeaveWord
	var leaveWords []models.LeaveWord
	all, err := c.o.QueryTable(leaveWord.TableName()).Filter("uid_id", uid).All(&leaveWords)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "数据库获取留言信息失败")
		c.ServeJSON()
		return
	}

	for i := 0; i < int(all); i++ {
		var user = models.User{Id: uid}
		err := c.o.Read(&user)
		if err != nil {
			c.Data["json"] = models.GetRevaErr(err, "数据库获取留言中的用户信息失败")
			c.ServeJSON()
			return
		}
		leaveWords[i].Uid = &user

		var visitor = models.Visitor{Id: leaveWords[i].Vid.Id}
		err = c.o.Read(&visitor)
		if err != nil {
			c.Data["json"] = models.GetRevaErr(err, "数据库获取留言中的访客信息失败")
			c.ServeJSON()
			return
		}
		leaveWords[i].Vid = &visitor

		var leaveWordTwo models.LeaveWordTwo
		var leaveWordTwos []models.LeaveWordTwo
		allLeaveWordTwos, err := c.o.QueryTable(leaveWordTwo.TableName()).Filter("leave_word_id_id", leaveWords[i]).All(&leaveWordTwos)
		if err != nil {
			c.Data["json"] = models.GetRevaErr(err, "数据库获取二级留言信息失败")
			c.ServeJSON()
			return
		}

		for j := 0; j < int(allLeaveWordTwos); j++ {
			var visitorTwo = models.Visitor{Id: leaveWordTwos[j].Vid.Id}
			err = c.o.Read(&visitorTwo)
			if err != nil {
				c.Data["json"] = models.GetRevaErr(err, "数据库获取留言中的访客信息失败")
				c.ServeJSON()
				return
			}
			leaveWordTwos[j].Vid = &visitorTwo
			leaveWords[i].LeaveWordTwo = append(leaveWords[i].LeaveWordTwo, &leaveWordTwos[j])
		}
	}
	c.Data["json"] = models.GetRevaOkAndDes(leaveWords, "共有"+strconv.Itoa(int(all))+"条留言")
	c.ServeJSON()
}

// GetLeaveWordTwoTest
// @Title	GetLeaveWordTwoTest
// @Description 查询二级留言结构体
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getLeaveWordTwoTest/ [get]
func (c *LeaveWordController) GetLeaveWordTwoTest() {
	var leaveWord = models.LeaveWordTwo{Id: 1, Word: "bnehikghiuq", Pass: 1, Vid: &models.Visitor{Id: 1}, LeaveWordId: &models.LeaveWord{Id: 1}}
	c.Data["json"] = models.GetRevaOk(leaveWord)
	c.ServeJSON()
}

// GetPassLeaveWordByUid
// @Title	GetPassLeaveWordByUid
// @Description 获取已经通过审核的留言
// @Param	uid 	path	int true	"用户id"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getPassLeaveWordByUid/:uid [get]
func (c *LeaveWordController) GetPassLeaveWordByUid() {
	uid, err := c.GetInt(":uid")

	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
		c.ServeJSON()
		return
	}

	var leaveWord models.LeaveWord
	var leaveWords []models.LeaveWord
	all, err := c.o.QueryTable(leaveWord.TableName()).Filter("uid_id", uid).Filter("pass", 1).All(&leaveWords)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "数据库获取留言信息失败")
		c.ServeJSON()
		return
	}

	for i := 0; i < int(all); i++ {
		var visitor = models.Visitor{Id: leaveWords[i].Vid.Id}
		err = c.o.Read(&visitor)
		if err != nil {
			c.Data["json"] = models.GetRevaErr(err, "数据库获取留言中的访客信息失败")
			c.ServeJSON()
			return
		}
		leaveWords[i].Vid = &visitor

		var leaveWordTwo models.LeaveWordTwo
		var leaveWordTwos []models.LeaveWordTwo
		allLeaveWordTwos, err := c.o.QueryTable(leaveWordTwo.TableName()).Filter("leave_word_id_id", leaveWords[i]).Filter("pass", 1).All(&leaveWordTwos)
		if err != nil {
			c.Data["json"] = models.GetRevaErr(err, "数据库获取二级留言信息失败")
			c.ServeJSON()
			return
		}

		for j := 0; j < int(allLeaveWordTwos); j++ {
			var visitorTwo = models.Visitor{Id: leaveWordTwos[j].Vid.Id}
			err = c.o.Read(&visitorTwo)
			if err != nil {
				c.Data["json"] = models.GetRevaErr(err, "数据库获取留言中的访客信息失败")
				c.ServeJSON()
				return
			}
			leaveWordTwos[j].Vid = &visitorTwo
			leaveWords[i].LeaveWordTwo = append(leaveWords[i].LeaveWordTwo, &leaveWordTwos[j])
		}
	}
	c.Data["json"] = models.GetRevaOkAndDes(leaveWords, "共有"+strconv.Itoa(int(all))+"条留言")
	c.ServeJSON()
}

// GetNotPassLeaveWordByUid
// @Title	GetNotPassLeaveWordByUid
// @Description 获取未通过审核的留言
// @Param	uid 	path	int true	"用户id"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getNotPassLeaveWordByUid/:uid [get]
func (c *LeaveWordController) GetNotPassLeaveWordByUid() {
	uid, err := c.GetInt(":uid")

	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
		c.ServeJSON()
		return
	}

	var leaveWord models.LeaveWord
	var leaveWords []models.LeaveWord
	all, err := c.o.QueryTable(leaveWord.TableName()).Filter("uid_id", uid).Filter("pass", 0).All(&leaveWords)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "数据库获取留言信息失败")
		c.ServeJSON()
		return
	}

	for i := 0; i < int(all); i++ {
		var visitor = models.Visitor{Id: leaveWords[i].Vid.Id}
		err = c.o.Read(&visitor)
		if err != nil {
			c.Data["json"] = models.GetRevaErr(err, "数据库获取留言中的访客信息失败")
			c.ServeJSON()
			return
		}
		leaveWords[i].Vid = &visitor
	}

	var passLeaveWords []models.LeaveWord
	allPassLeaveWord, err := c.o.QueryTable(leaveWord.TableName()).Filter("uid_id", uid).Filter("pass", 1).All(&passLeaveWords)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "数据库获取留言信息失败")
		c.ServeJSON()
		return
	}

	var allLeaveWords []models.LeaveWordTwo
	for i := 0; i < int(allPassLeaveWord); i++ {
		var leaveWordTwo models.LeaveWordTwo
		var leaveWordTwos []models.LeaveWordTwo
		allLeaveWordTwos, err := c.o.QueryTable(leaveWordTwo.TableName()).Filter("leave_word_id_id", passLeaveWords[i]).Filter("pass", 0).All(&leaveWordTwos)
		if err != nil {
			c.Data["json"] = models.GetRevaErr(err, "数据库获取二级留言信息失败")
			c.ServeJSON()
			return
		}

		for j := 0; j < int(allLeaveWordTwos); j++ {
			var visitorTwo = models.Visitor{Id: leaveWordTwos[j].Vid.Id}
			err = c.o.Read(&visitorTwo)
			if err != nil {
				c.Data["json"] = models.GetRevaErr(err, "数据库获取留言中的访客信息失败")
				c.ServeJSON()
				return
			}
			leaveWordTwos[j].Vid = &visitorTwo
		}
		allLeaveWords = append(allLeaveWords, leaveWordTwos...)
	}
	c.Data["json"] = models.GetRevaOk(struct {
		LeaveWord    []models.LeaveWord
		LeaveWordTwo []models.LeaveWordTwo
	}{
		leaveWords,
		allLeaveWords,
	})
	c.ServeJSON()
}

// UpdateLeaveWordStatus
// @Title	UpdateLeaveWordStatus
// @Description 修改一级留言状态
// @Param	lid 	fromdata	int true	"lid"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /updateLeaveWordStatus/ [put]
func (c *LeaveWordController) UpdateLeaveWordStatus() {
	get := c.Input().Get("lid")
	lid, err := strconv.Atoi(get)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "数据转换错误")
		c.ServeJSON()
		return
	}
	_, err = c.o.Update(&models.LeaveWord{Id: lid, Pass: 1}, "Pass")
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "修改留言状态失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOk("修改成功")
	c.ServeJSON()
}

// UpdateLeaveWordTowStatus
// @Title	UpdateLeaveWordTowStatus
// @Description 修改一级留言状态
// @Param	l2id 	fromdata	int true	"l2id"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /updateLeaveWordTowStatus/ [put]
func (c *LeaveWordController) UpdateLeaveWordTowStatus() {
	get := c.Input().Get("l2id")
	l2id, err := strconv.Atoi(get)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "数据转换错误")
		c.ServeJSON()
		return
	}
	_, err = c.o.Update(&models.LeaveWordTwo{Id: l2id, Pass: 1}, "Pass")
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "修改留言状态失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOk("修改成功")
	c.ServeJSON()
}
