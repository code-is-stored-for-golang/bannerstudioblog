package controllers

import (
	"bannerstudioblog/models"
)

type UserinfoController struct {
	baseController
}

// AddTag
// @Title	AddTag
// @Description 为用户新增一个标签
// @Param tagName formdate	string	true "tagName"
// @Param userinfoId formdate	int	true "userinfoId"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /addTag/ [post]
func (c *UserinfoController) AddTag() {
	tagName := c.GetString("tagName")
	userinfoId, err := c.GetInt("userinfoId")
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "用户信息id必须为数字")
		c.ServeJSON()
		return
	}
	var tag = models.Tag{TagName: tagName, Uid: &models.Userinfo{Id: userinfoId}}
	_, err = c.o.Insert(&tag)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "插入关于页标签失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOk("添加标签成功")
	c.ServeJSON()
	return
}

// DeleteTag
// @Title	DeleteTag
// @Description 为用户信息删除一个标签
// @Param Id path	int	true "tagId"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /deleteTag/:Id [delete]
func (c *UserinfoController) DeleteTag() {
	tagId, err := c.GetInt(":Id")
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "用户信息id必须为数字")
		c.ServeJSON()
		return
	}
	var tag = models.Tag{Id: tagId}
	_, err = c.o.Delete(&tag)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "删除关于页标签失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOk("删除标签成功")
	c.ServeJSON()
	return
}

// AddUserinfo
// @Title	AddUserinfo
// @Description 新增用户关于页信息
// @Param qq formdate	string	false "qq"
// @Param wechat formdate	String	false "wechat"
// @Param github formdate	string	false "github"
// @Param gitee formdate	string	false "gitee"
// @Param csdn formdate	String	false "csdn"
// @Param weibo formdate	String	false "weibo"
// @Param introduction formdate	String	false "introduction"
// @Param uid formdate	int	false "uid"
// @Success 201 增加成功
// @Failure 403 增加失败
// @router /addUserinfo/ [post]
func (c *UserinfoController) AddUserinfo() {
	qq := c.GetString("qq")
	wechat := c.GetString("wechat")
	github := c.GetString("github")
	gitee := c.GetString("gitee")
	csdn := c.GetString("csdn")
	weibo := c.GetString("weibo")
	introduction := c.GetString("introduction")
	uid, err := c.GetInt("uid")
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "用户id必须为数字")
		c.ServeJSON()
		return
	}
	if qq == "" {
		qq = "#"
	}
	if wechat == "" {
		wechat = "#"
	}
	if github == "" {
		github = "#"
	}
	if gitee == "" {
		gitee = "#"
	}
	if csdn == "" {
		csdn = "#"
	}
	if weibo == "" {
		weibo = "#"
	}
	if introduction == "" {
		introduction = "这位博主什么都没有留下"
	}
	var userinfo = models.Userinfo{Qq: qq, Wechat: wechat, Github: github, Gitee: gitee, Csdn: csdn, Weibo: weibo, Introduction: introduction, Uid: &models.User{Id: uid}}
	_, err = c.o.Insert(&userinfo)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "添加关于页信息失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOk("添加关于页信息成功")
	c.ServeJSON()
	return
}

// DeleteUserinfo
// @Title	DeleteTag
// @Description 删除用户关于页
// @Param userinfoId path	int	true "userinfoId"
// @Success 201 删除成功
// @Failure 403 删除失败
// @router /deleteUserinfo/:userinfoId [delete]
func (c *UserinfoController) DeleteUserinfo() {
	userinfoId, err := c.GetInt(":userinfoId")
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "用户信息id必须为数字")
		c.ServeJSON()
		return
	}
	var userinfo = models.Userinfo{Id: userinfoId}
	_, err = c.o.Delete(&userinfo)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "删除关于页失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOk("删除关于页成功")
	c.ServeJSON()
	return
}

// UpdateUserinfo
// @Title	UpdateUserinfo
// @Description 更改用户关于页信息
// @Param id formdate	string	false "id"
// @Param qq formdate	string	false "qq"
// @Param wechat formdate	String	false "wechat"
// @Param github formdate	string	false "github"
// @Param gitee formdate	string	false "gitee"
// @Param csdn formdate	String	false "csdn"
// @Param weibo formdate	String	false "weibo"
// @Param introduction formdate	String	false "introduction"
// @Param uid formdate	int	false "uid"
// @Success 201 更改成功
// @Failure 403 更改失败
// @router /updateUserinfo/ [put]
func (c *UserinfoController) UpdateUserinfo() {
	id, err := c.GetInt("id")
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "用户id必须为数字")
		c.ServeJSON()
		return
	}
	qq := c.GetString("qq")
	wechat := c.GetString("wechat")
	github := c.GetString("github")
	gitee := c.GetString("gitee")
	csdn := c.GetString("csdn")
	weibo := c.GetString("weibo")
	introduction := c.GetString("introduction")
	uid, err := c.GetInt("uid")
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "用户id必须为数字")
		c.ServeJSON()
		return
	}
	var userinfo1 = models.Userinfo{}
	err = c.o.QueryTable(userinfo1.TableName()).Filter("uid_id", uid).One(&userinfo1)
	if qq == "" {
		qq = userinfo1.Qq
	}
	if wechat == "" {
		wechat = userinfo1.Wechat
	}
	if github == "" {
		github = userinfo1.Github
	}
	if gitee == "" {
		gitee = userinfo1.Gitee
	}
	if csdn == "" {
		csdn = userinfo1.Csdn
	}
	if weibo == "" {
		weibo = userinfo1.Weibo
	}
	if introduction == "" {
		introduction = userinfo1.Introduction
	}
	var userinfo2 = models.Userinfo{Id: id, Qq: qq, Wechat: wechat, Github: github, Gitee: gitee, Csdn: csdn, Weibo: weibo, Introduction: introduction, Uid: &models.User{Id: uid}}
	_, err = c.o.Update(&userinfo2)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "修改关于页信息失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOk("修改关于页信息成功")
	c.ServeJSON()
	return
}

// GetUserinfoByUserId
// @Title	GetUserinfoByUserId
// @Description 查询关于页信息
// @Param uid path	int	true "uid"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getUserinfoByUserId/:uid [get]
func (c *UserinfoController) GetUserinfoByUserId() {
	uid, err := c.GetInt(":uid")
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "用户id必须为数字")
		c.ServeJSON()
		return
	}
	var userinfo = models.Userinfo{}
	err = c.o.QueryTable(userinfo.TableName()).Filter("uid_id", uid).One(&userinfo)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "查询关于页失败")
		c.ServeJSON()
		return
	}
	var tags []*models.Tag
	var tag models.Tag
	_, err = c.o.QueryTable(tag.TableName()).Filter("uid_id", userinfo.Id).All(&tags)
	if err != nil {
		c.Data["json"] = models.GetRevaErr(err, "查询关于页失败")
		c.ServeJSON()
		return
	}
	c.Data["json"] = models.GetRevaOkAndDes(map[string]interface{}{
		"userinfo": userinfo,
		"tags":     tags,
	}, "查询关于页成功")
	c.ServeJSON()
	return
}
