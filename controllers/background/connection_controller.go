package controllers

import (
	"bannerstudioblog/models"
	"encoding/json"
	"strconv"
)

type ConnectionController struct {
	baseController
}

// AddConnection
// @Title AddConnection
// @Description Add Connection information
// @Param body	body	models.Connection true	"Add Connection information"
// @Success 201 Add a Connection successfully
// @Failure 403 Failed to add a Connection
// @router /addConnection/ [post]
func (l *ConnectionController) AddConnection() {
	var conn models.Connection
	json.Unmarshal(l.Ctx.Input.RequestBody, &conn)
	_, err := l.o.Insert(&conn)
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		l.ServeJSON()
		return
	}
	l.Data["json"] = models.GetRevaOk(conn)
	l.ServeJSON()
}

// DeleteConnection
// @Title DeleteConnection
// @Description	Delete the Connection
// @Param id path	int	true "Delete Connection by ID"
// @Success 201	Succeeded in deleting the Connection by ID
// @Failure 403 Failed to delete Connection by ID
// @router /deleteConnection/:id [delete]
func (l *ConnectionController) DeleteConnection() {
	Id, err := l.GetInt(":id")
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
		l.ServeJSON()
		return
	}
	i, err := l.o.Delete(&models.Connection{Id: Id})
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		l.ServeJSON()
		return
	}
	l.Data["json"] = models.GetRevaOk("成功删除" + strconv.Itoa(int(i)) + "条数据")
	l.ServeJSON()
}

// UpdateConnection
// @Title UpdateConnection
// @Description Update the Connection
// @Param body body		models.Connection	true "Update the Connection"
// @Success 201	Update Connection successfully
// @Failure 403 Failed to update Connection
// @router /updateConnection/ [put]
func (l *ConnectionController) UpdateConnection() {
	var conn models.Connection
	json.Unmarshal(l.Ctx.Input.RequestBody, &conn)
	_, err := l.o.Update(&conn)
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		l.ServeJSON()
		return
	}
	l.Data["json"] = models.GetRevaOk(conn)
	l.ServeJSON()
}

// GetAllConnectionByUid
// @Title	GetAllConnectionByUid
// @Description 查询一个用户所有自设连接
// @Param	uid 	path	int true	"uid"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getAllConnectionByUid/:uid [get]
func (l *ConnectionController) GetAllConnectionByUid() {
	uid, err2 := l.GetInt(":uid")
	if err2 != nil {
		l.Data["json"] = models.GetRevaErr(err2, "接收参数失败")
		l.ServeJSON()
		return
	}

	var conns []models.Connection
	conn := models.Connection{}
	_, err := l.o.QueryTable(conn.TableName()).Filter("uid_id", uid).All(&conns)
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "获取全部链接失败")
		l.ServeJSON()
		return
	}
	l.Data["json"] = models.GetRevaOk(conns)
	l.ServeJSON()
}

// GetAllConnectionAndTitleByUid
// @Title	GetAllConnectionAndTitleByUid
// @Description 查询一个用户所有自设连接和标题
// @Param	uid 	path	int true	"uid"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getAllConnectionAndTitleByUid/:uid [get]
func (l *ConnectionController) GetAllConnectionAndTitleByUid() {
	uid, err2 := l.GetInt(":uid")
	if err2 != nil {
		l.Data["json"] = models.GetRevaErr(err2, "接收参数失败")
		l.ServeJSON()
		return
	}
	var user = models.User{Id: uid}
	err := l.o.Read(&user)
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "获取博客总标题失败")
		l.ServeJSON()
		return
	}
	var conns []models.Connection
	conn := models.Connection{}
	_, err = l.o.QueryTable(conn.TableName()).Filter("uid_id", uid).All(&conns)
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "获取全部链接失败")
		l.ServeJSON()
		return
	}
	l.Data["json"] = models.GetRevaOk(map[string]interface{}{
		"conns": conns,
		"title": user.Title,
	})
	l.ServeJSON()
}

// UpdateTitle
// @Title UpdateTitle
// @Description Update the title
// @Param title formdata		string	true "title"
// @Param uid formdata		int	true "uid"
// @Success 201	Update Connection successfully
// @Failure 403 Failed to update Connection
// @router /updateTitle/ [put]
func (l *ConnectionController) UpdateTitle() {
	title := l.GetString("title")
	uid, err := l.GetInt("uid")
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "接收数据失败")
		l.ServeJSON()
		return
	}
	var user = models.User{Id: uid, Title: title}
	_, err = l.o.Update(&user, "Title")
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		l.ServeJSON()
		return
	}
	l.Data["json"] = models.GetRevaOk("修改总标题成功")
	l.ServeJSON()
}
