package controllers

import (
	"bannerstudioblog/models"
	"encoding/json"
	"github.com/astaxie/beego/logs"
	"strconv"
	"time"
)

type VisitorsController struct {
	baseController
}

// GetVisitorInfo
// @Title	GetVisitorInfo
// @Description 查看一个访客信息
// @Param	id 	path	int true	"请输入需要查看的访客信息"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getVisitorInfo/:id [get]
func (v *VisitorsController) GetVisitorInfo() {
	id, err := v.GetInt(":id")
	if err != nil {
		v.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
		v.ServeJSON()
		return
	}
	visitor := models.Visitor{Id: id}
	if err := v.o.Read(&visitor); err != nil {
		v.Data["json"] = models.GetRevaErr(err, "从数据库获取信息失败")
		v.ServeJSON()
		return
	}
	v.Data["json"] = models.GetRevaOk(visitor)
	v.ServeJSON()
}

// GetAllVisitorInfo
// @Title	GetAllVisitorInfo
// @Description 查询所有访客信息
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getAllVisitorInfo/ [get]
func (v *VisitorsController) GetAllVisitorInfo() {
	var visitors []models.Visitor
	var visitor models.Visitor
	_, err := v.o.QueryTable(visitor.TableName()).All(&visitors)
	if err != nil {
		v.Data["json"] = models.GetRevaErr(err, "从数据库获取信息失败")
		v.ServeJSON()
		return
	}
	v.Data["json"] = models.GetRevaOk(visitors)
	v.ServeJSON()
}

// GetAllVisitorInfoByUid
// @Title	GetAllVisitorInfoByUid
// @Description 根据uid查询所有访客信息
// @Param	uid 	path	int true	"uid"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getAllVisitorInfo/:uid [get]
func (v *VisitorsController) GetAllVisitorInfoByUid() {
	uid, err2 := v.GetInt(":uid")
	logs.Info(uid)
	if err2 != nil {
		v.Data["json"] = models.GetRevaErr(err2, "获取参数")
		v.ServeJSON()
		return
	}
	var visitors []models.Visitor
	var visitor models.Visitor
	_, err := v.o.QueryTable(visitor.TableName()).Filter("user_id", uid).All(&visitors)
	if err != nil {
		v.Data["json"] = models.GetRevaErr(err, "从数据库获取信息失败")
		v.ServeJSON()
		return
	}
	v.Data["json"] = models.GetRevaOk(visitors)
	v.ServeJSON()
}

// AddVisitorInfo
// @Title	AddVisitorInfo
// @Description 上传访客信息
// @Param	visitor 	body	models.Visitor true	"请输入访客的json格式"
// @Success 201 插入成功
// @Failure 403 插入失败
// @router /addVisitorInfo/ [post]
func (v *VisitorsController) AddVisitorInfo() {
	var visitor models.Visitor
	err := json.Unmarshal(v.Ctx.Input.RequestBody, &visitor)
	if err != nil {
		v.Data["json"] = models.GetRevaErr(err, "从结构体中获取信息失败")
		v.ServeJSON()
		return
	}
	visitor.VTime = time.Now()
	id, err := v.o.Insert(&visitor)
	if err != nil {
		v.Data["json"] = models.GetRevaErr(err, "插入信息失败")
		v.ServeJSON()
		return
	}
	visitor.Id = int(id)
	v.Data["json"] = models.GetRevaOk(visitor)
	v.ServeJSON()
}

// UpdateVisitorInfo
// @Title	UpdateVisitorInfo
// @Description 更新访客信息
// @Param	visitor 	body	models.Visitor true	"根据id修改访客信息"
// @Success 201 修改成功
// @Failure 403 修改失败
// @router /updateVisitorInfo/ [put]
func (v *VisitorsController) UpdateVisitorInfo() {
	var visitor models.Visitor
	err := json.Unmarshal(v.Ctx.Input.RequestBody, &visitor)
	if err != nil {
		v.Data["json"] = models.GetRevaErr(err, "从结构体中获取信息失败")
		v.ServeJSON()
		return
	}
	var visitors = models.Visitor{Id: visitor.Id}
	err = v.o.Read(&visitors)
	if err != nil {
		v.Data["json"] = models.GetRevaErr(err, "更改失败")
		v.ServeJSON()
		return
	}
	visitor.VTime = visitors.VTime
	update, err := v.o.Update(&visitor)
	if err != nil {
		v.Data["json"] = models.GetRevaErr(err, "更改id为"+strconv.Itoa(int(update))+"的访客失败")
		v.ServeJSON()
		return
	}

	v.Data["json"] = models.GetRevaOk(visitor)
	v.ServeJSON()
}

// DeleteVisitorInfo
// @Title	DeleteVisitorInfo
// @Description 删除访客信息
// @Param	id 	path	int true	"输入要删除访客的id"
// @Success 201 删除成功
// @Failure 403 删除失败
// @router /deleteVisitorInfo/:id [delete]
func (v *VisitorsController) DeleteVisitorInfo() {
	id, err2 := v.GetInt(":id")
	if err2 != nil {
		v.Data["json"] = models.GetRevaErr(err2, "后台获取数据失败")
		v.ServeJSON()
		return
	}
	visitor := models.Visitor{Id: id}
	data, err := v.o.Delete(&visitor)
	if err != nil {
		v.Data["json"] = models.GetRevaErr(err, "删除信息失败")
		v.ServeJSON()
		return
	}
	v.Data["json"] = models.GetRevaOk("成功删除" + strconv.Itoa(int(data)) + "条数据")
	v.ServeJSON()
}

// GetVisitorJson
// @Title	GetVisitorJson
// @Description 查看一个访客信息Json格式
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getVisitorJson/ [get]
func (v *VisitorsController) GetVisitorJson() {
	visitor := models.Visitor{Id: 0}
	visitor.VTime = time.Now()
	visitor.VHomePage = "416246"
	visitor.VEmail = "4654"
	visitor.VName = "zhangsan"
	v.Data["json"] = models.GetRevaOk(visitor)
	v.ServeJSON()
}
