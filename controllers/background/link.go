package controllers

import (
	"bannerstudioblog/models"
	"encoding/json"
	"strconv"
)

type LinkController struct {
	baseController
}

//// GetLink
//// @Title	GetLink
//// @Description Query a link
//// @Param	id 	path	int true	"Query a Link by ID"
//// @Success 201 Query a link successfully
//// @Failure 403 Failed to query a link
//// @router /getLink/:id [get]
//func (l *LinkController) GetLink() {
//	Id, err := l.GetInt(":id")
//	if err != nil {
//		l.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
//		l.ServeJSON()
//		return
//	}
//	var link = models.Link{Id: Id}
//	err = l.o.Read(&link)
//	if err != nil {
//		l.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
//		l.ServeJSON()
//		return
//	}
//	l.Data["json"] = models.GetRevaOk(link)
//	l.ServeJSON()
//}
//
//// @Title	GetAllLink
//// @Description Query all links
//// @Success 201 Query all links successfully
//// @Failure 403 Failed to query all links
//// @router /getAllLink/ [get]
//func (l *LinkController) GetAllLink() {
//	var links []models.Link
//	link := models.Link{}
//	_, err := l.o.QueryTable(link.TableName()).All(&links)
//	if err != nil {
//		l.Data["json"] = models.GetRevaErr(err, "获取全部链接失败")
//		l.ServeJSON()
//		return
//	}
//	l.Data["json"] = models.GetRevaOk(links)
//	l.ServeJSON()
//}

// AddLink
// @Title AddLink
// @Description Add link information
// @Param body	body	models.Link true	"Add link information"
// @Success 201 Add a link successfully
// @Failure 403 Failed to add a link
// @router /addLink/ [post]
func (l *LinkController) AddLink() {
	var link models.Link
	json.Unmarshal(l.Ctx.Input.RequestBody, &link)
	_, err := l.o.Insert(&link)
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		l.ServeJSON()
		return
	}
	l.Data["json"] = models.GetRevaOk(link)
	l.ServeJSON()
}

// DeleteLink
// @Title DeleteLink
// @Description	Delete the link
// @Param id path	int	true "Delete link by ID"
// @Success 201	Succeeded in deleting the link by ID
// @Failure 403 Failed to delete link by ID
// @router /deleteLink/:id [delete]
func (l *LinkController) DeleteLink() {
	Id, err := l.GetInt(":id")
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "后台获取数据失败")
		l.ServeJSON()
		return
	}
	i, err := l.o.Delete(&models.Link{Id: Id})
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		l.ServeJSON()
		return
	}
	l.Data["json"] = models.GetRevaOk("成功删除" + strconv.Itoa(int(i)) + "条数据")
	l.ServeJSON()
}

// UpdateLink
// @Title UpdateLink
// @Description Update the link
// @Param body body		models.Link	true "Update the link"
// @Success 201	Update link successfully
// @Failure 403 Failed to update link
// @router /updateLink/ [put]
func (l *LinkController) UpdateLink() {
	var link models.Link
	json.Unmarshal(l.Ctx.Input.RequestBody, &link)
	_, err := l.o.Update(&link)
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "后台操作数据库失败")
		l.ServeJSON()
		return
	}
	l.Data["json"] = models.GetRevaOk(link)
	l.ServeJSON()
}

// GetAllLinkByUid
// @Title	GetAllLinkByUid
// @Description 查询一个用户所有连接
// @Param	uid 	path	int true	"uid"
// @Success 201 查询成功
// @Failure 403 查询失败
// @router /getAllLinkByUid/:uid [get]
func (l *LinkController) GetAllLinkByUid() {
	uid, err2 := l.GetInt(":uid")
	if err2 != nil {
		l.Data["json"] = models.GetRevaErr(err2, "接收参数失败")
		l.ServeJSON()
		return
	}

	var links []models.Link
	link := models.Link{}
	_, err := l.o.QueryTable(link.TableName()).Filter("uid_id", uid).All(&links)
	if err != nil {
		l.Data["json"] = models.GetRevaErr(err, "获取全部链接失败")
		l.ServeJSON()
		return
	}
	l.Data["json"] = models.GetRevaOk(links)
	l.ServeJSON()
}
