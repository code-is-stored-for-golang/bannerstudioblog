package models

// Userinfo 用户信息
type Userinfo struct {
	Id              int    `json:"id" orm:"pk;auto"`
	IntroductoryTag []*Tag `json:"introductory_tag" orm:"reverse(many)"`
	// 社交连接
	Qq           string `json:"qq"`
	Wechat       string `json:"wechat"`
	Github       string `json:"github"`
	Gitee        string `json:"gitee"`
	Csdn         string `json:"csdn"`
	Weibo        string `json:"weibo"`
	Introduction string `json:"introduction" orm:"type(text)"`
	Uid          *User  `json:"uid" orm:"rel(fk)"`
}

func (u *Userinfo) TableName() string {
	return TableName("userinfo")
}
