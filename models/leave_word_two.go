package models

import "time"

type LeaveWordTwo struct {
	Id          int        `json:"id" orm:"pk;auto"`
	Word        string     `json:"word"`
	Pass        int        `json:"pass"`
	Vid         *Visitor   `json:"vid" orm:"rel(fk)"`
	MassageTime time.Time  `json:"massage_time"`
	ReplyTo     string     `json:"reply_to"`
	LeaveWordId *LeaveWord `json:"leave_word_id" orm:"rel(fk)"`
}

func (u *LeaveWordTwo) TableName() string {
	return TableName("leave_word_two")
}
