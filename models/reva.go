package models

type Reva struct {
	Err		error
	Msg		interface{}
	Code	int
	Describe	string
}

func GetRevaOk(Msg interface{}) Reva {
	return Reva{Code: 200,Msg: Msg}
}

func GetRevaOkAndDes(Msg interface{},Describe string) Reva {
	return Reva{Code: 200,Msg: Msg,Describe: Describe}
}

func GetRevaErr(Err error,Describe string) Reva {
	return Reva{Code: 500,Err: Err,Describe: Describe}
}
