package models

type Signature struct {
	Id        int    `json:"id" orm:"pk;auto"`
	Signature string `json:"signature"`
	SpaceName string `json:"space_name"`
	Uid       *User  `json:"uid" orm:"rel(fk)"`
}

func (s *Signature) TableName() string {
	return TableName("signature")
}
