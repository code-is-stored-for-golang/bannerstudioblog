// Package models
// @program: BannerStudioBlog.background
// @author: Hyy
// @create: 2021-10-28 17:10
package models

import (
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
)

type User struct {
	Id               int      `json:"id" orm:"pk;auto"`
	Username         string   `json:"username"`
	Password         string   `json:"password"`
	HeadPortrait     string   `json:"head_portrait"`
	Nickname         string   `json:"nickname"`
	Theme            string   `json:"theme"`
	Title            string   `json:"title"`
	DomainNameSuffix string   `json:"domain_name_suffix"`
	Blog             []*Blog  `json:"blog" orm:"reverse(many)"`
	Types            []*Type  `json:"types" orm:"reverse(many)"`
	Labels           []*Label `json:"labels" orm:"reverse(many)"`
}

func (u *User) TableName() string {
	return TableName("user")
}

// DoCreateUser
// @Title DoCreateUser
// @Description "创建用户"
// @Param  cr 	*CreateRequest 		"用户创建请求"
// @return    *CreateResponse       "用户创建响应"
// @return    int        			"状态码"
// @return    error         		"错误信息"
func DoCreateUser(cr *User) bool {
	// 连接数据库
	o := orm.NewOrm()
	// 检查用户名是否存在
	var userNameCheck User
	err := o.QueryTable("tab_user").Filter("username", cr.Username).One(&userNameCheck)
	if err == nil {
		return false
	}
	_, err = o.Insert(&cr)
	if err != nil {
		logs.Error(err.Error())
		return false
	}
	return true
}
