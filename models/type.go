package models

type Type struct {
	Id       int    `json:"id" orm:"pk;auto"`
	TypeName string `json:"type_name"`
	Bid      *Blog  `json:"bid" orm:"rel(fk)"`
	Uid      *User  `json:"uid" orm:"rel(fk)"`
}

func (u *Type) TableName() string {
	return TableName("type")
}
