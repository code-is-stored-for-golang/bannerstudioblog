package models

type Connection struct {
	Id             int    `json:"id" orm:"pk;auto"`
	ConnectionName string `json:"connection_name"`
	Connection     string `json:"connection"`
	Uid            *User  `json:"uid" orm:"rel(fk)"`
}

func (c *Connection) TableName() string {
	return TableName("conn")
}
