package models

type AssistObj struct {
	Blog   Blog     `json:"blog"`
	Types  []string `json:"types"`
	Labels []string `json:"labels"`
}
