package models

type Link struct {
	Id          int    `json:"id" orm:"pk;auto"`
	LinkName    string `json:"link_name"`
	Link        string `json:"link"`
	LinkSummary string `json:"link_summary"`
	Uid         *User  `json:"uid" orm:"rel(fk)"`
}

func (l *Link) TableName() string {
	return TableName("link")
}
