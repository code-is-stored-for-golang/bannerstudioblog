package models

type Label struct {
	Id        int    `json:"id" orm:"pk;auto"`
	LabelName string `json:"label_name"`
	Bid       *Blog  `json:"bid" orm:"rel(fk)"`
	Uid       *User  `json:"uid" orm:"rel(fk)"`
}

func (u *Label) TableName() string {
	return TableName("label")
}
