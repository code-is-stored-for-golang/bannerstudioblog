package models

// Tag 用户信息简介标签
type Tag struct {
	Id      int       `json:"id" orm:"pk;auto"`
	TagName string    `json:"tag_name"`
	Uid     *Userinfo `json:"uid" orm:"rel(fk)"`
}

func (t *Tag) TableName() string {
	return TableName("tag")
}
