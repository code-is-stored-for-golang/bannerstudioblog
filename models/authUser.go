package models

type AuthUser struct {
	Id                    int
	User                  string
	Password              string
	Email                 string
	Phone                 string
	Role                  string
	MemberName            string
	Sex                   string
	Direction             string
	Grade                 string
	Birthday              string
	HeadPortraitUrl       string
	BlogUrl               string
	GitUrl                string
	PersonalizedSignature string
	MemberQQ              string
	MemberWeChat          string
	MemberCompany         string
	MemberWork            string
	MemberAddress         string
	MemberPay             int
	PersonalIntroduction  string
}
