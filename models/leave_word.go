package models

import "time"

type LeaveWord struct {
	Id           int             `json:"id" orm:"pk;auto"`
	Word         string          `json:"word"`
	Pass         int             `json:"pass"`
	Vid          *Visitor        `json:"vid" orm:"rel(fk)"`
	Uid          *User           `json:"uid" orm:"rel(fk)"`
	MassageTime  time.Time       `json:"massage_time"`
	LeaveWordTwo []*LeaveWordTwo `json:"leave_word_two" orm:"reverse(many)"`
}

func (u *LeaveWord) TableName() string {
	return TableName("leave_word")
}
