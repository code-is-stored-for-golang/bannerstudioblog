package models

import "time"

type Visitor struct {
	Id            int       `json:"id" orm:"pk;auto"`
	VName         string    `json:"v_name"`
	VEmail        string    `json:"v_email"`
	VHeadPortrait string    `json:"v_head_portrait"`
	VHomePage     string    `json:"v_home_page"`
	VTime         time.Time `json:"v_time"`
	IsAuthor      int8      `json:"is_author"`
	User          *User     `json:"user" orm:"rel(fk)"`
}

func (u *Visitor) TableName() string {
	return TableName("visitor")
}
