package main

import (
	"bannerstudioblog/models"
	_ "bannerstudioblog/routers"
	"bannerstudioblog/utils"
	"github.com/astaxie/beego"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	//utils.Init()
	models.Init()
	utils.Init()
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	beego.Run()
}
