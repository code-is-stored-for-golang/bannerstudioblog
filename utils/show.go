package utils

import (
	"github.com/russross/blackfriday"
	"regexp"
	"strings"
)

// TrimHtml 去除HTML标签
func TrimHtml(html string) string {
	//将HTML标签全转换成小写
	re, _ := regexp.Compile("\\<[\\S\\s]+?\\>")
	html = re.ReplaceAllStringFunc(html, strings.ToLower)
	//去除STYLE
	re, _ = regexp.Compile("\\<style[\\S\\s]+?\\</style\\>")
	html = re.ReplaceAllString(html, "")
	//去除SCRIPT
	re, _ = regexp.Compile("\\<script[\\S\\s]+?\\</script\\>")
	html = re.ReplaceAllString(html, "")
	//去除所有尖括号内的HTML代码，并换成换行符
	re, _ = regexp.Compile("\\<[\\S\\s]+?\\>")
	html = re.ReplaceAllString(html, "\n")
	//去除连续的换行符
	re, _ = regexp.Compile("\\s+")
	html = re.ReplaceAllString(html, "")
	return strings.TrimSpace(html)
}

// Markdown2Html Markdown format to HTML format
func Markdown2Html(markdown string) string {
	html := blackfriday.MarkdownCommon([]byte(markdown))
	return string(html)
}

// StringSliceWeightRemoval
// 字符串切片去重
func StringSliceWeightRemoval(ss []string) (s []string) {
	tmpMap := make(map[string]interface{})
	for _, val := range ss {
		//判断主键为val的map是否存在
		if _, ok := tmpMap[val]; !ok {
			s = append(s, val)
			tmpMap[val] = nil
		}
	}
	return
}
