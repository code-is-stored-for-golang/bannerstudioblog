package utils

import (
	"bannerstudioblog/models"
	"encoding/json"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"strconv"
)

func Init() {
	interceptor()

	filter()

	listener()
}

// listener	监听器
func listener() {
	// 添加日志拦截器
	//var FilterLog = func(ctx *context.Context) {
	//	url, _ := json.Marshal(ctx.Input.Data()["RouterPattern"])
	//	params, _ := json.Marshal(ctx.Request.Form)
	//	outputBytes, _ := json.Marshal(ctx.Input.Data()["json"])
	//	divider := " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	//	topDivider := "┌" + divider
	//	middleDivider := "├" + divider
	//	bottomDivider := "└" + divider
	//	outputStr := "\n" + topDivider + "\n│ 请求地址:" + string(url) + "\n" + middleDivider + "\n│ 请求参数:" + string(params) + "\n│ 返回数据:" + string(outputBytes) + "\n" + bottomDivider
	//	logs.Info(outputStr)
	//}

	// 打印请求头里的数据
	//var FilterLog = func(ctx *context.Context) {
	//	marshal:= ctx.Input.Header("token")
	//	validateToken, _ := ValidateToken(marshal)
	//	logs.Info(validateToken["userName"])
	//}
	//// 最后一个参数必须设置为false 不然无法打印数据
	//beego.InsertFilter("/*", beego.FinishRouter, FilterLog, false)

	//这玩意不能用  逻辑就那样了，获取token 解析一下 看看是否有效就行
	//beego.InsertFilter("/*",beego.BeforeRouter, func(ctx *context.Context) {
	//	token:=ctx.Input.Header("token")
	//	_,err:=utils.ValidateToken(token)
	//	if err != nil||token=="" {
	//		ctx.Redirect(http.StatusTemporaryRedirect,"http://localhost:8081/api/user/login")
	//	}
	//},true,false)

	beego.InsertFilter("/api/blog/GetBlog/:id([0-9]+)", beego.FinishRouter, func(c *context.Context) {
		var err error
		params, _ := json.Marshal(c.Request.RequestURI[18:])
		id, err := strconv.Atoi(string(params))
		if err != nil {
			_, err = c.ResponseWriter.Write([]byte("给id为" + strconv.Itoa(id) + "的博客添加浏览量失败"))
			//beego.Error(err)
		}
		var blog models.Blog
		// 增加博客的浏览量
		err = blog.AddViews(id)
		if err != nil {
			_, err = c.ResponseWriter.Write([]byte("增加该博客浏览量失败"))
			return
		}
		err = blog.AddDayViews()
		if err != nil {
			_, err = c.ResponseWriter.Write([]byte("增加该日浏览量失败"))
			return
		}
		err = blog.AddAllViews()
		if err != nil {
			_, err = c.ResponseWriter.Write([]byte("增加总浏览量失败"))
			return
		}
		//beego.Error("增加成功")
	}, false)
}

// filter 过滤器
func filter() {
	//var AdminLogin = func(ctx *context.Context) {
	//	marshal := ctx.Input.Header("token")
	//	validateToken, _ := ValidateToken(marshal)
	//	var authUser models.AuthUser
	//	jsonUser, err := json.Marshal(validateToken)
	//	if err != nil {
	//		logs.Info(err)
	//		return
	//	}
	//	err = json.Unmarshal(jsonUser, &authUser)
	//	if err != nil {
	//		logs.Info(err)
	//		return
	//	}
	//	logs.Info(authUser)
	//}
	//beego.InsertFilter("/*", beego.FinishRouter, AdminLogin, false)

}

// interceptor	拦截器
func interceptor() {

}
