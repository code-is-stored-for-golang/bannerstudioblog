package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "AddBlog",
			Router:           `/AddBlog/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "DeleteBlog",
			Router:           `/DeleteBlog/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetAllArticleByDomainNameSuffix",
			Router:           `/GetAllArticleByDomainNameSuffix/:domainNameSuffix`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetAllArticleByUid",
			Router:           `/GetAllArticleByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetAllBlogByUid",
			Router:           `/GetAllBlogByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetAllDraftByUid",
			Router:           `/GetAllDraftByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetBlog",
			Router:           `/GetBlog/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetViewsInWeek",
			Router:           `/GetViewsInWeek/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "UpdateBlog",
			Router:           `/UpdateBlog/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetAllArticle",
			Router:           `/getAllArticle/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetAllBlog",
			Router:           `/getAllBlog/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetAllBlogViews",
			Router:           `/getAllBlogViews/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetAllDraft",
			Router:           `/getAllDraft/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetBlogByLabel",
			Router:           `/getBlogByLabel/:uid/:label`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetBlogByType",
			Router:           `/getBlogByType/:uid/:type`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetBlogViews",
			Router:           `/getBlogViews/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetBlogsCount",
			Router:           `/getBlogsCount/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:BlogController"],
		beego.ControllerComments{
			Method:           "GetBlogsCountByUid",
			Router:           `/getBlogsCountByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:ConnectionController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:ConnectionController"],
		beego.ControllerComments{
			Method:           "AddConnection",
			Router:           `/addConnection/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:ConnectionController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:ConnectionController"],
		beego.ControllerComments{
			Method:           "DeleteConnection",
			Router:           `/deleteConnection/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:ConnectionController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:ConnectionController"],
		beego.ControllerComments{
			Method:           "GetAllConnectionByUid",
			Router:           `/getAllConnectionByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:ConnectionController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:ConnectionController"],
		beego.ControllerComments{
			Method:           "UpdateConnection",
			Router:           `/updateConnection/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "GetAssist",
			Router:           `/AddLabels/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "AddLabel",
			Router:           `/addLabel/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "AddType",
			Router:           `/addType/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "DeleteLabel",
			Router:           `/delLabel/`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "DeleteType",
			Router:           `/delType/`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "GetLabelsAndCountByUid",
			Router:           `/getLabelsAndCountByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "GetLabelsByDomainNameSuffix",
			Router:           `/getLabelsByDomainNameSuffix/:DomainNameSuffix`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "GetLabelsByUid",
			Router:           `/getLabelsByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "GetTypesAndCountByUid",
			Router:           `/getTypesAndCountByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "GetTypesByDomainNameSuffix",
			Router:           `/getTypesByDomainNameSuffix/:DomainNameSuffix`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "GetTypesByUid",
			Router:           `/getTypesByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "UpdateLabel",
			Router:           `/updateLabel/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "UpdateLabelByOldLabelName",
			Router:           `/updateLabelByOldLabelName/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "UpdateType",
			Router:           `/updateType/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LabelController"],
		beego.ControllerComments{
			Method:           "UpdateTypeNameByOldTypeName",
			Router:           `/updateTypeNameByOldTypeName/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"],
		beego.ControllerComments{
			Method:           "AddLeaveWord",
			Router:           `/addLeaveWord/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"],
		beego.ControllerComments{
			Method:           "AddLeaveWordTwo",
			Router:           `/addLeaveWordTwo/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"],
		beego.ControllerComments{
			Method:           "DeleteLeaveWordById",
			Router:           `/deleteLeaveWordById/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"],
		beego.ControllerComments{
			Method:           "DeleteLeaveWordTwoById",
			Router:           `/deleteLeaveWordTwoById/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"],
		beego.ControllerComments{
			Method:           "GetLeaveWordByUid",
			Router:           `/getLeaveWordByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"],
		beego.ControllerComments{
			Method:           "GetLeaveWordTwoTest",
			Router:           `/getLeaveWordTwoTest/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"],
		beego.ControllerComments{
			Method:           "GetNotPassLeaveWordByUid",
			Router:           `/getNotPassLeaveWordByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"],
		beego.ControllerComments{
			Method:           "GetPassLeaveWordByUid",
			Router:           `/getPassLeaveWordByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"],
		beego.ControllerComments{
			Method:           "UpdateLeaveWordStatus",
			Router:           `/updateLeaveWordStatus/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LeaveWordController"],
		beego.ControllerComments{
			Method:           "UpdateLeaveWordTowStatus",
			Router:           `/updateLeaveWordTowStatus/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LinkController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LinkController"],
		beego.ControllerComments{
			Method:           "AddLink",
			Router:           `/addLink/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LinkController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LinkController"],
		beego.ControllerComments{
			Method:           "DeleteLink",
			Router:           `/deleteLink/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LinkController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LinkController"],
		beego.ControllerComments{
			Method:           "GetAllLinkByUid",
			Router:           `/getAllLinkByUid/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LinkController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:LinkController"],
		beego.ControllerComments{
			Method:           "UpdateLink",
			Router:           `/updateLink/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:SignatureController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:SignatureController"],
		beego.ControllerComments{
			Method:           "AddSignature",
			Router:           `/addSignature/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:SignatureController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:SignatureController"],
		beego.ControllerComments{
			Method:           "DeleteSignature",
			Router:           `/deleteSignature/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:SignatureController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:SignatureController"],
		beego.ControllerComments{
			Method:           "GetSignature",
			Router:           `/getSignature/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:SignatureController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:SignatureController"],
		beego.ControllerComments{
			Method:           "UpdateSignature",
			Router:           `/updateSignature/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:TenCloudCosController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:TenCloudCosController"],
		beego.ControllerComments{
			Method:           "DeleteFile",
			Router:           `/deleteFile/:fileName`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:TenCloudCosController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:TenCloudCosController"],
		beego.ControllerComments{
			Method:           "Upload",
			Router:           `/upload`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"],
		beego.ControllerComments{
			Method:           "AddUserinfo",
			Router:           `/AddUserinfo/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"],
		beego.ControllerComments{
			Method:           "DeleteUserinfoById",
			Router:           `/DeleteUserinfoById/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"],
		beego.ControllerComments{
			Method:           "GetUserinfoByDomainNameSuffix",
			Router:           `/GetUserinfoByDomainNameSuffix/:DomainNameSuffix`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"],
		beego.ControllerComments{
			Method:           "GetUserinfoById",
			Router:           `/GetUserinfoById/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"],
		beego.ControllerComments{
			Method:           "UpdateUserinfo",
			Router:           `/UpdateUserinfo/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"],
		beego.ControllerComments{
			Method:           "ParseToken",
			Router:           `/getAuth`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"],
		beego.ControllerComments{
			Method:           "Login",
			Router:           `/login`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserController"],
		beego.ControllerComments{
			Method:           "GetToken",
			Router:           `/login/callback`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"],
		beego.ControllerComments{
			Method:           "AddTag",
			Router:           `/addTag/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"],
		beego.ControllerComments{
			Method:           "AddUserinfo",
			Router:           `/addUserinfo/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"],
		beego.ControllerComments{
			Method:           "DeleteTag",
			Router:           `/deleteTag/:Id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"],
		beego.ControllerComments{
			Method:           "DeleteUserinfo",
			Router:           `/deleteUserinfo/:userinfoId`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"],
		beego.ControllerComments{
			Method:           "GetUserinfoByUserId",
			Router:           `/getUserinfoByUserId/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:UserinfoController"],
		beego.ControllerComments{
			Method:           "UpdateUserinfo",
			Router:           `/updateUserinfo/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"],
		beego.ControllerComments{
			Method:           "AddVisitorInfo",
			Router:           `/addVisitorInfo/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"],
		beego.ControllerComments{
			Method:           "DeleteVisitorInfo",
			Router:           `/deleteVisitorInfo/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"],
		beego.ControllerComments{
			Method:           "GetAllVisitorInfo",
			Router:           `/getAllVisitorInfo/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"],
		beego.ControllerComments{
			Method:           "GetAllVisitorInfoByUid",
			Router:           `/getAllVisitorInfo/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"],
		beego.ControllerComments{
			Method:           "GetVisitorInfo",
			Router:           `/getVisitorInfo/:id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"],
		beego.ControllerComments{
			Method:           "GetVisitorJson",
			Router:           `/getVisitorJson/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"] = append(beego.GlobalControllerRouter["bannerstudioblog/controllers/background:VisitorsController"],
		beego.ControllerComments{
			Method:           "UpdateVisitorInfo",
			Router:           `/updateVisitorInfo/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

}
