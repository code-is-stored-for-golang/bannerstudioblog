// Package routers @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	controllers "bannerstudioblog/controllers/background"
	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/api",
		beego.NSNamespace("/user",
			beego.NSInclude(
				&controllers.UserController{},
			),
		),
		beego.NSNamespace("/blog",
			beego.NSInclude(
				&controllers.BlogController{},
			),
		),
		beego.NSNamespace("/link",
			beego.NSInclude(
				&controllers.LinkController{},
			),
		),
		beego.NSNamespace("/signature",
			beego.NSInclude(
				&controllers.SignatureController{},
			),
		),
		beego.NSNamespace("/tencent_cloud",
			beego.NSInclude(
				&controllers.TenCloudCosController{},
			),
		),
		beego.NSNamespace("/label",
			beego.NSInclude(
				&controllers.LabelController{},
			),
		),
		beego.NSNamespace("/visitor",
			beego.NSInclude(
				&controllers.VisitorsController{},
			),
		),
		beego.NSNamespace("/leave_word",
			beego.NSInclude(
				&controllers.LeaveWordController{},
			),
		),
		beego.NSNamespace("/conn",
			beego.NSInclude(
				&controllers.ConnectionController{},
			),
		),
		beego.NSNamespace("/userinfo",
			beego.NSInclude(
				&controllers.UserinfoController{},
			),
		),
	)
	beego.AddNamespace(ns)
}
